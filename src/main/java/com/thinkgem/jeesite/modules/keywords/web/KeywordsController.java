/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.keywords.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.keywords.entity.KeyWords;
import com.thinkgem.jeesite.modules.keywords.service.KeyWordsService;

/**
 * 关键词管理Controller
 * @author dingshuang
 * @version 2016-07-10
 */
@Controller
@RequestMapping(value = "${adminPath}/keywords/keyWords")
public class KeywordsController extends BaseController {

    @Autowired
    private KeyWordsService keyWordsService;

    @ModelAttribute
    public KeyWords get(@RequestParam(required=false) String id) {
        KeyWords entity = null;
        if (StringUtils.isNotBlank(id)){
            entity = keyWordsService.get(id);
        }
        if (entity == null){
            entity = new KeyWords();
        }
        return entity;
    }

    @RequiresPermissions("keywords:keyWords:view")
    @RequestMapping(value = {"list", ""})
    public String list(KeyWords keyWords, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<KeyWords> page = keyWordsService.findPage(new Page<KeyWords>(request, response), keyWords);
        model.addAttribute("page", page);
        return "modules/keywords/keyWordsList";
    }

    @RequiresPermissions("keywords:keyWords:view")
    @RequestMapping(value = "form")
    public String form(KeyWords keyWords, Model model) {
        model.addAttribute("keyWords", keyWords);
        return "modules/keywords/keyWordsForm";
    }

    @RequiresPermissions("keywords:keyWords:edit")
    @RequestMapping(value = "save")
    public String save(KeyWords keyWords, Model model, RedirectAttributes redirectAttributes) {
        if (!beanValidator(model, keyWords)){
            return form(keyWords, model);
        }
        keyWordsService.save(keyWords);
        addMessage(redirectAttributes, "保存关键词管理成功");
        return "redirect:"+Global.getAdminPath()+"/keywords/keyWords/?repage";
    }

    @RequiresPermissions("keywords:keyWords:edit")
    @RequestMapping(value = "delete")
    public String delete(KeyWords keyWords, RedirectAttributes redirectAttributes) {
        keyWordsService.delete(keyWords);
        addMessage(redirectAttributes, "删除关键词管理成功");
        return "redirect:"+Global.getAdminPath()+"/keywords/keyWords/?repage";
    }

}