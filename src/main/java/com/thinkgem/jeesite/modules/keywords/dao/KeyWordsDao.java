/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.keywords.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.keywords.entity.KeyWords;

import java.util.List;

/**
 * 关键词管理DAO接口
 * @author dingshuang
 * @version 2016-07-10
 */
@MyBatisDao
public interface KeyWordsDao extends CrudDao<KeyWords> {
    public List<KeyWords> getUseableWords();
}