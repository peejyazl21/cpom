/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.keywords.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.keywords.entity.KeyWords;
import com.thinkgem.jeesite.modules.keywords.dao.KeyWordsDao;

/**
 * 关键词管理Service
 * @author dingshuang
 * @version 2016-07-10
 */
@Service
@Transactional(readOnly = true)
public class KeyWordsService extends CrudService<KeyWordsDao, KeyWords> {

	@Autowired
	private KeyWordsDao keyWordsDao;

	public KeyWords get(String id) {
		return super.get(id);
	}
	
	public List<KeyWords> findList(KeyWords keyWords) {
		return super.findList(keyWords);
	}
	
	public Page<KeyWords> findPage(Page<KeyWords> page, KeyWords keyWords) {
		return super.findPage(page, keyWords);
	}
	
	@Transactional(readOnly = false)
	public void save(KeyWords keyWords) {
		super.save(keyWords);
	}
	
	@Transactional(readOnly = false)
	public void delete(KeyWords keyWords) {
		super.delete(keyWords);
	}

	public List<KeyWords> getUseableWords(){
		return keyWordsDao.getUseableWords();
	}
	
}