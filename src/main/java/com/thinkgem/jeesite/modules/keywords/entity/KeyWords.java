/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.keywords.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 关键词管理Entity
 * @author dingshuang
 * @version 2016-07-10
 */
public class KeyWords extends DataEntity<KeyWords> {
	
	private static final long serialVersionUID = 1L;
	private String word;		// 关键词
	private String usable;		// 是否可用
	
	public KeyWords() {
		super();
	}

	public KeyWords(String id){
		super(id);
	}

	@Length(min=0, max=200, message="关键词长度必须介于 0 和 200 之间")
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	
	@Length(min=0, max=1, message="是否可用长度必须介于 0 和 1 之间")
	public String getUsable() {
		return usable;
	}

	public void setUsable(String usable) {
		this.usable = usable;
	}
	
}