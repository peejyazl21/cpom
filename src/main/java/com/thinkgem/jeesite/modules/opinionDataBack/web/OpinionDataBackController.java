/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionDataBack.web;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import com.thinkgem.jeesite.modules.opinionDataBack.service.OpinionDataBackService;
import com.thinkgem.jeesite.modules.opinionWarning.entity.OpinionWarning;
import com.thinkgem.jeesite.modules.opinionmonitoring.entity.OpinionMonitoring;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 数据回收Controller
 *
 * @author dingshuang
 * @version 2016-06-21
 */
@Controller
@RequestMapping(value = "${adminPath}/opinionDataBack/opinionDataBack")
public class OpinionDataBackController extends BaseController {

    @Autowired
    private OpinionDataBackService opinionDataBackService;

    /**
     * 跳转到研判数据回收页面（主页）
     *
     * @param page
     * @param model
     * @return
     */
    @RequiresPermissions("opinionDataBack:opinionDataBack:view")
    @RequestMapping(value = {"list", "judgeList"})
    public String judgeList(Page<CollectData> page,CollectData entity,HttpServletRequest request, Model model) {
        page.setPageSize(5);
        page = opinionDataBackService.getDeleteCollectDataPage(page,entity);
        model.addAttribute("page",page);
        model.addAttribute("title",request.getParameter("title"));
        return "modules/opinionDataBack/judgeList";
    }

    /**
     * 跳转到监控数据回收页面
     *
     * @param page
     * @param model
     * @return
     */
    @RequiresPermissions("opinionDataBack:opinionDataBack:view")
    @RequestMapping(value = {"monitoringList", ""})
    public String monitoringList(Page<OpinionMonitoring> page, OpinionMonitoring entity ,Model model) {
        page.setPageSize(5);
        page = opinionDataBackService.getDeleteMonitoringOpinionPage(page,entity);
        model.addAttribute("page",page);
        return "modules/opinionDataBack/monitoringList";
    }


    /**
     * 跳转到预警数据回收页面
     *
     * @param page
     * @param model
     * @return
     */
    @RequiresPermissions("opinionDataBack:opinionDataBack:view")
    @RequestMapping(value = {"warningList", ""})
    public String warningList(Page<OpinionWarning> page,OpinionWarning entity, Model model) {
        page.setPageSize(5);
        page = opinionDataBackService.getDeleteOpinionWarningPage(page,entity);
        model.addAttribute("page",page);
        return "modules/opinionDataBack/warningList";
    }





    /**
     * 数据回复
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "dataBack")
    public void dataBack(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter out = response.getWriter();
        String id = request.getParameter("id");
        String dataTag = request.getParameter("dataTag");
        if("1".equals(dataTag)){
            //恢复研判数据
            opinionDataBackService.judgeDataBack(id);
        }else if("2".equals(dataTag)){
            //恢复监控数据
            opinionDataBackService.monitoringDataBack(id);
        }else if("3".equals(dataTag)){
            //恢复预警数据
            opinionDataBackService.warningDataBack(id);
        }
        out.write(JSONObject.valueToString(1).toString());
    }

    /**
     *
     * @Author 管正爽
     * @Time 2016-7-28
     * @备注 重点监控回收
     */
    @RequiresPermissions("opinionDataBack:opinionDataBack:view")
    @RequestMapping(value = {"opinionMonitoringList", ""})
    public String opinionMonitoringList(Page<OpinionMonitoring> page, OpinionMonitoring entity ,Model model) {
        page.setPageSize(5);
        page = opinionDataBackService.getOpinionMonitoringPage(page,entity);
        model.addAttribute("page",page);
        return "modules/opinionDataBack/opinionMonitoringList";
    }

    /**
     *
     * @Author 管正爽
     * @Time 2016-7-28
     * @备注 一般监控回收
     */
    @RequiresPermissions("opinionDataBack:opinionDataBack:view")
    @RequestMapping(value = {"generalMonitoringList", ""})
    public String generalMonitoringList(Page<OpinionMonitoring> page, OpinionMonitoring entity ,Model model) {
        page.setPageSize(5);
        page = opinionDataBackService.getGeneralMonitoringPage(page,entity);
        model.addAttribute("page",page);
        return "modules/opinionDataBack/generalMonitoringList";
    }
}