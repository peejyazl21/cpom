/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionsynergy.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 舆情协办信息管理Entity
 * @author dingshuang
 * @version 2016-06-21
 */
public class PublicOpinionSynergy extends DataEntity<PublicOpinionSynergy> {
	
	private static final long serialVersionUID = 1L;
	private String publicOpinionId;		// 舆情编号
	private Date synergizeDate;		// 协办时间
	private String synergizeDeptCode;		// 协办部门代码
	private String synergizeContent;		// 协办内容
	private String synergizeAtta;		// 协办附件

	public PublicOpinionSynergy() {
		super();
	}

	public PublicOpinionSynergy(String id){
		super(id);
	}

	@Length(min=0, max=64, message="舆情编号长度必须介于 0 和 64 之间")
	public String getPublicOpinionId() {
		return publicOpinionId;
	}

	public void setPublicOpinionId(String publicOpinionId) {
		this.publicOpinionId = publicOpinionId;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getSynergizeDate() {
		return synergizeDate;
	}

	public void setSynergizeDate(Date synergizeDate) {
		this.synergizeDate = synergizeDate;
	}
	
	@Length(min=0, max=10, message="协办部门代码长度必须介于 0 和 10 之间")
	public String getSynergizeDeptCode() {
		return synergizeDeptCode;
	}

	public void setSynergizeDeptCode(String synergizeDeptCode) {
		this.synergizeDeptCode = synergizeDeptCode;
	}
	
	public String getSynergizeContent() {
		return synergizeContent;
	}

	public void setSynergizeContent(String synergizeContent) {
		this.synergizeContent = synergizeContent;
	}

	public String getSynergizeAtta() {
		return synergizeAtta;
	}

	public void setSynergizeAtta(String synergizeAtta) {
		this.synergizeAtta = synergizeAtta;
	}
}