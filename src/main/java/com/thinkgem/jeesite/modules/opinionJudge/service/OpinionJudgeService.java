/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionJudge.service;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.collectdata.dao.CollectDataDao;
import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import com.thinkgem.jeesite.modules.opinioncheck.dao.PublicOpinionCheckDao;
import com.thinkgem.jeesite.modules.opinioncheck.entity.PublicOpinionCheck;
import com.thinkgem.jeesite.modules.opinionsynergy.dao.PublicOpinionSynergyDao;
import com.thinkgem.jeesite.modules.opinionsynergy.entity.PublicOpinionSynergy;
import com.thinkgem.jeesite.modules.publicoption.dao.PublicOpinionDao;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;
import com.thinkgem.jeesite.modules.publicoption.util.PublicOpinionConstant;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 舆情研判Service
 * @author dingshuang
 * @version 2016-06-21
 */
@Service
@Transactional(readOnly = true)
public class OpinionJudgeService extends CrudService<PublicOpinionDao, PublicOpinion> {

	@Autowired
	private CollectDataDao  collectDataDao;

	@Transactional(readOnly = false)
	public CollectData getCollectData(CollectData collectData){
		return collectDataDao.get(collectData);
	}

	@Transactional(readOnly = false)
	public void updateCollectData(CollectData collectData){
		collectDataDao.update(collectData);
	}


	@Transactional(readOnly = false)
	public void deleteCollectData(CollectData collectData){
		collectDataDao.delete(collectData);
	}

}