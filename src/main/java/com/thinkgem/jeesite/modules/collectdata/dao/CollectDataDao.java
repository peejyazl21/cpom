/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.collectdata.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 采集数据管理DAO接口
 * @author dingshuang
 * @version 2016-07-10
 */
@MyBatisDao
public interface CollectDataDao extends CrudDao<CollectData> {

    List<CollectData> getCollectDataNotIndex();

    List<CollectData>  getCollectDataByURL(Map<String,Object> map);

    CollectData getCollectDataNewCollect(@Param("originUrl") String originUrl);

    CollectData getCollectDataDurationNewCollect(Map<String,Object> map);

    int getGroupIsIndexCount(@Param("originUrl") String originUrl);

    List<CollectData>  findDeleteCollectDataList(CollectData entity);

    int reverseDelete(CollectData entity);

}