/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionWarning.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.opinionWarning.entity.OpinionWarning;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**W
 * 舆情预警DAO接口
 * @author dingshuang
 * @version 2016-07-12
 */
@MyBatisDao
public interface OpinionWarningDao extends CrudDao<OpinionWarning> {

    int getOpinionWarningCount(@Param("originUrl")String originUrl);

    int getCount();

    List<OpinionWarning> getDeleteOpinionWarningPage(OpinionWarning entity);

    int reverseDelete(OpinionWarning entity);


}