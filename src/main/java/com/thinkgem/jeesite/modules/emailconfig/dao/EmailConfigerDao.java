/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.emailconfig.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.emailconfig.entity.EmailConfiger;

/**
 * 邮箱信息配置DAO接口
 * @author dingshuang
 * @version 2016-06-15
 */
@MyBatisDao
public interface EmailConfigerDao extends CrudDao<EmailConfiger> {
	
}