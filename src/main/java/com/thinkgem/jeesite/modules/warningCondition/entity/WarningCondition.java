/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.warningCondition.entity;

import org.hibernate.validator.constraints.Length;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 预警条件Entity
 * @author dingshuang
 * @version 2016-07-12
 */
public class WarningCondition extends DataEntity<WarningCondition> {
	
	private static final long serialVersionUID = 1L;
	private String func;		// 表示预警条件的函数
	private String param;		// 参数（多个用；分割）
	private String isUsed;		// is_used
	
	public WarningCondition() {
		super();
	}

	public WarningCondition(String id){
		super(id);
	}

	@Length(min=0, max=20, message="表示预警条件的函数长度必须介于 0 和 20 之间")
	public String getFunc() {
		return func;
	}

	public void setFunc(String func) {
		this.func = func;
	}
	
	@Length(min=0, max=100, message="参数（多个用；分割）长度必须介于 0 和 100 之间")
	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}
	
	@Length(min=0, max=1, message="is_used长度必须介于 0 和 1 之间")
	public String getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(String isUsed) {
		this.isUsed = isUsed;
	}
	
}