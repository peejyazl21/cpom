/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.warningCondition.web;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;
import com.thinkgem.jeesite.modules.warningCondition.entity.WarningCondition;
import com.thinkgem.jeesite.modules.warningCondition.service.WarningConditionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 舆情预警Controller
 * @author dingshuang
 * @version 2016-06-21
 */
@Controller
@RequestMapping(value = "${adminPath}/warningCondition/warningCondition")
public class WarningConditionController extends BaseController {

	@Autowired
	private WarningConditionService warningConditionService;

	/**
	 * 跳转舆情预警页面
	 * @param model
     * @return
     */
	@RequiresPermissions("warningCondition:warningCondition:view")
	@RequestMapping(value = {"list", ""})
	public String list(Model model) {
		return "modules/warningCondition/warningConditionList";
	}


	/**
	 * 预警条件保存
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "save")
	public void save(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String warningParams = request.getParameter("warningParams");
		PrintWriter out = response.getWriter();
		int result  = warningConditionService.saveWarningParams(warningParams);
		out.write(JSONObject.valueToString(result).toString());
	}



	/**
	 * 获取预警条件
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "jsonData")
	public void jsonData(HttpServletRequest request, HttpServletResponse response) throws IOException {
		List<WarningCondition>  list =  warningConditionService.findList(new WarningCondition());
		List<Map<String,Object>> jsonList = new ArrayList<Map<String,Object>>();
		if(list!=null && list.size()>0){
			for (WarningCondition warningCondition:list) {
				Map<String,Object> map= new HashMap<String,Object>();
				map.put("id",warningCondition.getId());
				map.put("func",warningCondition.getFunc());
				map.put("isUsed",warningCondition.getIsUsed());
				map.put("param",warningCondition.getParam());
				jsonList.add(map);
			}
		}
		String jsonData = JSONObject.valueToString(jsonList).toString();
		PrintWriter out = response.getWriter();
		out.write(jsonData);
	}


}