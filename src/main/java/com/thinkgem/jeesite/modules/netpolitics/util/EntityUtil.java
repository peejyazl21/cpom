package com.thinkgem.jeesite.modules.netpolitics.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/***
 * 获取实体属性以实体属性值
 *
 * @author dingshuang
 * @version v1.3
 * @date 2013-1-4 上午10:25:02
 * @company
 * @since 相关/版本
 */
public class EntityUtil {

    private static Logger logger = LoggerFactory.getLogger(EntityUtil.class);

    public static Map<String, Object> getDeclaredFieldsToMap(Object model, String superFields[]) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 获取实体类的所有属性，返回Field数组
        Field[] field = model.getClass().getDeclaredFields();
        // 遍历所有属性
        for (int j = 0; j < field.length; j++) {
            try {
                // 获取属性的名字
                String name = field[j].getName();
                // 将属性的首字符大写，方便构造get，set方法
                Method m = model.getClass().getMethod("get" + name.substring(0, 1).toUpperCase() + name.substring(1));
                Object value = m.invoke(model);
                map.put(name, value);
            } catch (Exception e) {
                logger.info("数据获取出现错误");
            }
        }
        if (superFields != null) {
            for (int i = 0; i < superFields.length; i++) {
                try {
                    String name = superFields[0];
                    Method m = model.getClass().getMethod("get" + name.substring(0, 1).toUpperCase() + name.substring(1));
                    map.put(name, m.invoke(model));

                } catch (Exception e) {
                    logger.info("数据获取出现错误");
                }
            }
        }
        //补充需要的的属性，比如id（在super中）
        return map;
    }


    /**
     * 获取实体属性以实体属性值
     *
     * @param list
     * @return
     */
    public static List<Map<String, Object>> getMainProperty(List list, String superFields[]) {
        List<Map<String, Object>> resultList = new ArrayList<Map<String, Object>>();
        if (list != null && list.size() > 0) {
            for (Object entity : list) {
                Map<String, Object> map = EntityUtil.getDeclaredFieldsToMap(entity, superFields);
                resultList.add(map);
            }
        }
        return resultList;
    }


}
