package com.thinkgem.jeesite.modules.netpolitics.util;

import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.Message.RecipientType;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Calendar;
import java.util.Properties;

public class EmailUtil {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EmailUtil.class);
	/**
	 * 该方法用于邮件发送
	 * @param subject
	 * @param messageText
	 * @param toAddr
	 */
	public static void sendMail(String server, String from, String fpassword, String toAddr, String subject, String messageText, String port) {
		try {
			EmailUtil.sendMessage(server, from, fpassword, toAddr, subject, messageText, "text/html;charset=gb2312", port);
		} catch (MessagingException e) {
			logger.info("对不起，邮件发送失败！！！");
			logger.info(e.getMessage());
		}
	}

	/**
	 * 该方法用于邮件发送操作
	 */
	@SuppressWarnings("static-access")
	private static void sendMessage(String smtpHost, String from, String fromUserPassword, String to, String subject,
			String messageText, String messageType, String port) throws MessagingException {
		// 第一步：配置javax.mail.Session对象
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.starttls.enable", "true");// 使用 STARTTLS安全连接
		props.put("mail.smtp.port", port); // google使用465或587端口
		props.put("mail.smtp.auth", "true"); // 使用验证
		Session mailSession = Session.getInstance(props, new MyAuthenticator(from, fromUserPassword));

		// 第二步：编写消息
		InternetAddress fromAddress = new InternetAddress(from);
		InternetAddress toAddress = new InternetAddress(to);
		MimeMessage message = new MimeMessage(mailSession);
		message.setFrom(fromAddress);
		message.addRecipient(RecipientType.TO, toAddress);
		message.setSentDate(Calendar.getInstance().getTime());
		message.setSubject(subject);
		message.setContent(messageText, messageType);

		// 第三步：发送消息
		Transport transport = mailSession.getTransport("smtp");
		transport.connect(smtpHost, "347611964", fromUserPassword);
		transport.send(message, message.getRecipients(RecipientType.TO));
	}

	public static void main(String[] args) {
		String port = "25";// 端口
		String server = "smtp.qq.com"; // 邮件服务器
		String from = "84571523@qq.com"; // 发送者
		String fpassword = "md123456";
		String to = "347611964@qq.com";// 发送者地址
		String subject = "测试邮件";// 主题
		String messageText = "测试邮件测试邮件测试邮件";// 正文
		String contentType = "text/html;charset=gb2312";// 正文类型
		try {
			EmailUtil.sendMessage(server, from, fpassword, to, subject, messageText, contentType, port);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}

// 用于邮箱验证的类
class MyAuthenticator extends Authenticator {
	private String userName = "";
	private String password = "";

	public MyAuthenticator(String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(userName, password);
	}
}
