/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.publicoption.service;

import com.google.common.collect.Maps;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.act.dao.ActDao;
import com.thinkgem.jeesite.modules.act.entity.Act;
import com.thinkgem.jeesite.modules.act.service.ActTaskService;
import com.thinkgem.jeesite.modules.act.utils.ActUtils;
import com.thinkgem.jeesite.modules.opinioncheck.dao.PublicOpinionCheckDao;
import com.thinkgem.jeesite.modules.opinioncheck.entity.PublicOpinionCheck;
import com.thinkgem.jeesite.modules.opinionsynergy.dao.PublicOpinionSynergyDao;
import com.thinkgem.jeesite.modules.opinionsynergy.entity.PublicOpinionSynergy;
import com.thinkgem.jeesite.modules.publicoption.dao.PublicOpinionDao;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;
import com.thinkgem.jeesite.modules.publicoption.util.PublicOpinionConstant;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 舆情监控Service
 * @author dingshuang
 * @version 2016-06-21
 */
@Service
@Transactional(readOnly = true)
public class PublicOpinionService extends CrudService<PublicOpinionDao, PublicOpinion> {

	@Autowired
	private ActTaskService actTaskService;

	@Autowired
	private PublicOpinionSynergyDao publicOpinionSynergyDao;

	@Autowired
	private PublicOpinionCheckDao publicOpinionCheckDao;


	public PublicOpinion get(String id) {
		return super.get(id);
	}
	
	public List<PublicOpinion> findList(PublicOpinion publicOpinion) {
		return super.findList(publicOpinion);
	}
	
	public Page<PublicOpinion> findPage(Page<PublicOpinion> page, PublicOpinion publicOpinion) {
//		publicOpinion.getSqlMap().put("dsf", dataScopeFilter(UserUtils.getUser().getCurrentUser(), "o", "a"));
		return super.findPage(page, publicOpinion);
	}
	
	@Transactional(readOnly = false)
	public void save(PublicOpinion publicOpinion) {
		Map<String, Object> vars = Maps.newHashMap();
		if("hand_out".equals(publicOpinion.getAction())){ //当前的保存动作是分发
			publicOpinion.setStatus("2");//状态修改为处理中
			// 流程任务参数
			String account = getProcessAccountByOfficeId(publicOpinion.getOffice().getId());
			vars.put("handler",account);//舆情信息办理人员
			publicOpinion.getAct().setComment("请"+publicOpinion.getOffice().getName()+"理办");
		}else if("join_hands".equals(publicOpinion.getAction())){       //完成协办
			PublicOpinionSynergy publicOpinionSynergy = new PublicOpinionSynergy();
			publicOpinionSynergy.setPublicOpinionId(publicOpinion.getId());
			publicOpinionSynergy.setSynergizeDate(new Date());
			publicOpinionSynergy.setSynergizeContent(StringEscapeUtils.unescapeHtml4(publicOpinion.getSynergizeContent()));
			publicOpinionSynergy.setSynergizeAtta(publicOpinion.getSynergizeAtta());
			publicOpinionSynergy.setSynergizeDeptCode(UserUtils.getUser().getOffice().getCode());
			publicOpinionSynergy.preInsert();
			publicOpinionSynergyDao.insert(publicOpinionSynergy);
			// 流程任务参数（上次任务的办理人）
			vars.put("handler",this.getPreTaskAsignee(publicOpinion.getProcessInstanceId()));
		}else if("deal_with_0".equals(publicOpinion.getAction())){       //退回到分发操作
			// 流程任务参数
			vars.put("msg","0"); //流转到退回操作
			vars.put("handler",PublicOpinionConstant.USER_XXZX_ACCOUNT);
		}else if("deal_with_1".equals(publicOpinion.getAction())){        //提交审核
			vars.put("msg","3");
			vars.put("handler",PublicOpinionConstant.USER_XXZX_ACCOUNT);
		}else if("deal_with_2".equals(publicOpinion.getAction())){ //要求协办
			// 流程任务参数
			vars.put("msg","1"); //流转到协办操作
			String account = getProcessAccountByOfficeId(publicOpinion.getOffice().getId());
			vars.put("handler",account);//舆情信息办理
		}else if("check_action_1".equals(publicOpinion.getAction())){ //信息审核通过，办结
			publicOpinion.setStatus("3");//状态修改为已办理
			vars.put("msg","1");
		}else if("check_action_0".equals(publicOpinion.getAction())){ //信息审核不通过，退回
			vars.put("msg","0");
			//保存审核信息
			PublicOpinionCheck publicOpinionCheck = new PublicOpinionCheck();
			publicOpinionCheck.setReason(StringEscapeUtils.unescapeHtml4(publicOpinion.getUncheckedReason()));
			publicOpinionCheck.setCheckDate(new Date());
			publicOpinionCheck.setOpinionId(publicOpinion.getId());
			publicOpinionCheck.preInsert();
			publicOpinionCheckDao.insert(publicOpinionCheck);
			vars.put("handler",this.getPreTaskAsignee(publicOpinion.getProcessInstanceId()));
		}
		publicOpinion.setReplyContent(StringEscapeUtils.unescapeHtml4(publicOpinion.getReplyContent()));
		super.save(publicOpinion);
		actTaskService.complete(publicOpinion.getAct().getTaskId(), publicOpinion.getAct().getProcInsId(), publicOpinion.getAct().getComment(), publicOpinion.getTitle(), vars);
	}
	
	@Transactional(readOnly = false)
	public void delete(PublicOpinion publicOpinion) {
		super.delete(publicOpinion);
	}


	/**
	 * 舆情信息转入
	 * @param publicOpinion
	 * @return
     */
	@Transactional(readOnly = false)
	public int publicOpinionInto(PublicOpinion publicOpinion ){
		if (StringUtils.isBlank(publicOpinion.getId())){
			publicOpinion.preInsert();
			dao.insert(publicOpinion);
			// 启动流程
			Map<String, Object> vars = Maps.newHashMap();
			//舆情信息初始被转入,处理部门为信息中心
			vars.put("handler", PublicOpinionConstant.USER_XXZX_ACCOUNT);
			publicOpinion.getAct().setComment("暂无");
			actTaskService.startProcess(ActUtils.PD_PUBLIC_OPINION[0], ActUtils.PD_PUBLIC_OPINION[1], publicOpinion.getId(), publicOpinion.getTitle(),vars);
		}
		return 1;
	}

	@Transactional(readOnly = false)
	public void publicOpinionInsert(PublicOpinion publicOpinion){
		super.save(publicOpinion);
	}



	/**
	 * 填充协办信息列表
	 * @param publicOpinion
	 * @return
     */
	 public PublicOpinion setPublicOpinionSynergys(PublicOpinion publicOpinion){
		 List<PublicOpinionSynergy> lsit= publicOpinionSynergyDao.getListByPublicOpinion(publicOpinion);
		 publicOpinion.setSynergylist(lsit);
		 return publicOpinion;
	 }


	/**
	 * 填充审核信息列表
	 * @param publicOpinion
	 * @return
	 */
	public PublicOpinion setPublicOpinionchecks(PublicOpinion publicOpinion){
		List<PublicOpinionCheck> list= publicOpinionCheckDao.getListByPublicOpinion(publicOpinion);
		publicOpinion.setChecklist(list);
		return publicOpinion;
	}


	/**
	 * 通过机构id获取流程参与用户
	 * @param officeId
	 * @return
     */
	private String getProcessAccountByOfficeId(String officeId){
		if(StringUtils.isNotBlank(officeId)){
			officeId=officeId.split(",")[0];
		}
		String account = dao.getProcessAccountByOfficeId(officeId);
		return account;
	}


	/**
	 * 获取上一步流程的操作者
	 * @param procInsId
	 * @return
     */
	private String  getPreTaskAsignee(String procInsId){
		String assignee = PublicOpinionConstant.USER_XXZX_ACCOUNT;
		if(StringUtils.isNotBlank(procInsId)){
			List<Act> list = actTaskService.histoicFlowList(procInsId,"","");
			if(list!=null&&list.size()>0){
				Act act = list.get((list.size()-2));
				for (Act _act:list) {
					System.out.println("办理人顺序："+_act.getAssignee());
				}
				assignee = act.getAssignee();
			}
		}
		return assignee;
	}


}