/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.publicoption.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;
import com.thinkgem.jeesite.modules.sys.entity.User;
import org.apache.ibatis.annotations.Param;

/**
 * 舆情监控DAO接口
 * @author dingshuang
 * @version 2016-06-21
 */
@MyBatisDao
public interface PublicOpinionDao extends CrudDao<PublicOpinion> {

    public String getProcessAccountByOfficeId(@Param("officeId") String officeId);
	
}