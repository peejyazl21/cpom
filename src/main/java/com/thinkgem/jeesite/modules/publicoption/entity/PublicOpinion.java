/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.publicoption.entity;

import com.thinkgem.jeesite.common.persistence.ActEntity;
import com.thinkgem.jeesite.modules.opinioncheck.entity.PublicOpinionCheck;
import com.thinkgem.jeesite.modules.opinionsynergy.entity.PublicOpinionSynergy;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.thinkgem.jeesite.common.persistence.DataEntity;

/**
 * 舆情监控Entity
 * @author dingshuang
 * @version 2016-06-21
 */
public class PublicOpinion extends ActEntity<PublicOpinion> {
	
	private static final long serialVersionUID = 1L;
	private String processInstanceId;		// 流程实例编号
	private String collectOrigin;		// 采集源
	private String originUrl;		// 源地址
	private String title;		// 标题
	private String content;		// 内容
	private String releaseTime;		// 发布时间
	private String viewCount;		// 浏览量
	private String transCount;		// 转发量
	private String replyCount;		// 回复量
	private String involveField;		// 涉及领域
	private String involveTownName;		// 涉及街镇
	private String involveTownCode;		// 涉及街镇代码
	private String status;		// 状态
	private String replyDate;		// 回复时间
	private String replyDeptName;		// 回复部门名称
	private String replyDeptCode;		// 回复部门代码
	private String replyContent;		// 回复内容
	private String replyProve;		// 回复证明
	private String uncheckedReason;//审核不通过理由

	public Office office;

	private String action;

	private String synergizeContent;

	private String synergizeAtta;

	private List<PublicOpinionSynergy> synergylist;

	private List<PublicOpinionCheck> checklist;

	
	public PublicOpinion() {
		super();
	}

	public PublicOpinion(String id){
		super(id);
	}

	@Length(min=0, max=64, message="流程实例编号长度必须介于 0 和 64 之间")
	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	
	@Length(min=0, max=100, message="采集源长度必须介于 0 和 100 之间")
	public String getCollectOrigin() {
		return collectOrigin;
	}

	public void setCollectOrigin(String collectOrigin) {
		this.collectOrigin = collectOrigin;
	}
	
	@Length(min=0, max=200, message="源地址长度必须介于 0 和 200 之间")
	public String getOriginUrl() {
		return originUrl;
	}

	public void setOriginUrl(String originUrl) {
		this.originUrl = originUrl;
	}
	
	@Length(min=0, max=200, message="标题长度必须介于 0 和 200 之间")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	

	public String getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}
	
	@Length(min=0, max=10, message="浏览量长度必须介于 0 和 10 之间")
	public String getViewCount() {
		return viewCount;
	}

	public void setViewCount(String viewCount) {
		this.viewCount = viewCount;
	}
	
	@Length(min=0, max=10, message="转发量长度必须介于 0 和 10 之间")
	public String getTransCount() {
		return transCount;
	}

	public void setTransCount(String transCount) {
		this.transCount = transCount;
	}
	
	@Length(min=0, max=10, message="回复量长度必须介于 0 和 10 之间")
	public String getReplyCount() {
		return replyCount;
	}

	public void setReplyCount(String replyCount) {
		this.replyCount = replyCount;
	}
	
	@Length(min=0, max=2, message="涉及领域长度必须介于 0 和 2 之间")
	public String getInvolveField() {
		return involveField;
	}

	public void setInvolveField(String involveField) {
		this.involveField = involveField;
	}
	
	@Length(min=0, max=200, message="涉及街镇长度必须介于 0 和 200 之间")
	public String getInvolveTownName() {
		return involveTownName;
	}

	public void setInvolveTownName(String involveTownName) {
		this.involveTownName = involveTownName;
	}
	
	@Length(min=0, max=2, message="涉及街镇代码长度必须介于 0 和 2 之间")
	public String getInvolveTownCode() {
		return involveTownCode;
	}

	public void setInvolveTownCode(String involveTownCode) {
		this.involveTownCode = involveTownCode;
	}
	
	@Length(min=0, max=2, message="状态长度必须介于 0 和 2 之间")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReplyDate() {
		return replyDate;
	}

	public void setReplyDate(String replyDate) {
		this.replyDate = replyDate;
	}
	
	@Length(min=0, max=200, message="回复部门名称长度必须介于 0 和 200 之间")
	public String getReplyDeptName() {
		return replyDeptName;
	}

	public void setReplyDeptName(String replyDeptName) {
		this.replyDeptName = replyDeptName;
	}
	
	@Length(min=0, max=10, message="回复部门代码长度必须介于 0 和 10 之间")
	public String getReplyDeptCode() {
		return replyDeptCode;
	}

	public void setReplyDeptCode(String replyDeptCode) {
		this.replyDeptCode = replyDeptCode;
	}
	
	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}
	
	@Length(min=0, max=200, message="回复证明长度必须介于 0 和 200 之间")
	public String getReplyProve() {
		return replyProve;
	}

	public void setReplyProve(String replyProve) {
		this.replyProve = replyProve;
	}


	public String getUncheckedReason() {
		return uncheckedReason;
	}

	public void setUncheckedReason(String uncheckedReason) {
		this.uncheckedReason = uncheckedReason;
	}

	public Office getOffice() {
		return office;
	}

	public void setOffice(Office office) {
		this.office = office;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}


	public String getSynergizeContent() {
		return synergizeContent;
	}

	public void setSynergizeContent(String synergizeContent) {
		this.synergizeContent = synergizeContent;
	}

	public String getSynergizeAtta() {
		return synergizeAtta;
	}

	public void setSynergizeAtta(String synergizeAtta) {
		this.synergizeAtta = synergizeAtta;
	}


	public List<PublicOpinionSynergy> getSynergylist() {
		return synergylist;
	}

	public void setSynergylist(List<PublicOpinionSynergy> synergylist) {
		this.synergylist = synergylist;
	}


	public List<PublicOpinionCheck> getChecklist() {
		return checklist;
	}

	public void setChecklist(List<PublicOpinionCheck> checklist) {
		this.checklist = checklist;
	}
}