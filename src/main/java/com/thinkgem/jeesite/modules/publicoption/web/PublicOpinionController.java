/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.publicoption.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.netpolitics.entity.PoliticalMail;
import com.thinkgem.jeesite.modules.netpolitics.util.EntityUtil;
import com.thinkgem.jeesite.modules.sys.entity.Office;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;
import com.thinkgem.jeesite.modules.publicoption.service.PublicOpinionService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 舆情监控Controller
 * @author dingshuang
 * @version 2016-06-21
 */
@Controller
@RequestMapping(value = "${adminPath}/publicoption/publicOpinion")
public class PublicOpinionController extends BaseController {

	@Autowired
	private PublicOpinionService publicOpinionService;
	
	@ModelAttribute
	public PublicOpinion get(@RequestParam(required=false) String id) {
		PublicOpinion entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = publicOpinionService.get(id);
		}
		if (entity == null){
			entity = new PublicOpinion();
		}
		return entity;
	}
	
	@RequiresPermissions("publicoption:publicOpinion:view")
	@RequestMapping(value = {"list", ""})
	public String list(PublicOpinion publicOpinion, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<PublicOpinion> page = publicOpinionService.findPage(new Page<PublicOpinion>(request, response), publicOpinion); 
		model.addAttribute("page", page);
		return "modules/publicoption/publicOpinionList";
	}

	@RequiresPermissions("publicoption:publicOpinion:view")
	@RequestMapping(value = "form")
	public String form(PublicOpinion publicOpinion, Model model) {
		String view = "publicOpinionDetail";
		if("todo".equals(publicOpinion.getAct().getStatus())){
			view = "publicOpinionForm";//跳转至办理页面
			if (StringUtils.isNotBlank(publicOpinion.getId())) {
				// 环节编号
				String taskDefKey = publicOpinion.getAct().getTaskDefKey();
				if("usertask1".equals(taskDefKey)){
					//信息分发 环节
					publicOpinion.setAction("hand_out");
				}
				if("usertask2".equals(taskDefKey)){
					//部门办理 环节
					publicOpinion.setAction("deal_with");
				}
				if("usertask3".equals(taskDefKey)){
					//部门协办 环节
					publicOpinion.setAction("join_hands");
				}
				if("usertask4".equals(taskDefKey)){
					//信息审核 环节
					publicOpinion.setAction("to_check");
				}
			}
		}
		//填充协办信息列表
		publicOpinion = publicOpinionService.setPublicOpinionSynergys(publicOpinion);
		//填充审核信息列表
		publicOpinion = publicOpinionService.setPublicOpinionchecks(publicOpinion);
		model.addAttribute("publicOpinion", publicOpinion);
		return "modules/publicoption/"+view;
	}

	@RequiresPermissions("publicoption:publicOpinion:edit")
	@RequestMapping(value = "save")
	public String save(PublicOpinion publicOpinion, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, publicOpinion)){
			return form(publicOpinion, model);
		}
		publicOpinionService.save(publicOpinion);
		addMessage(redirectAttributes, "保存舆情监控成功");
//		return "redirect:"+Global.getAdminPath()+"/publicoption/publicOpinion/?repage";
		return "redirect:"+Global.getAdminPath()+"/act/task/todo/?repage";
	}

	@RequiresPermissions("publicoption:publicOpinion:edit")
	@RequestMapping(value = "delete")
	public String delete(PublicOpinion publicOpinion, RedirectAttributes redirectAttributes) {
		publicOpinionService.delete(publicOpinion);
		addMessage(redirectAttributes, "删除舆情监控成功");
		return "redirect:"+Global.getAdminPath()+"/publicoption/publicOpinion/?repage";
	}


	/**
	 * 该方法支持异步jsonp跨域获取所有相关的信息
	 *
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "jspon")
	public void jspon(HttpServletRequest request, HttpServletResponse response,PublicOpinion publicOpinion) throws IOException {
		PrintWriter out = response.getWriter();
		String callbackName = request.getParameter("callbackParam");
		String function = request.getParameter("function");
		Object result = null; // 返回数据结果
		if ("publicOpinionInto".equals(function)) {
			//舆情信息转入方法
			result = publicOpinionService.publicOpinionInto(publicOpinion);
		}
		if("getpublicOpinion".equals(function)){
			//获取舆情信息
		}
		out.write(callbackName + "(" + JSONObject.valueToString(result).toString() + ")");
	}




	/**
	 * 导出excel数据
	 * @param request
	 * @param response
	 * @throws IOException
     */
	@RequestMapping(value = "exportExcel")
	public void exportExcel(HttpServletRequest request, HttpServletResponse response) throws IOException {
		PrintWriter out = response.getWriter();
        String result ="/download/excel/20160714.xlsx";
		out.write(JSONObject.valueToString(result).toString());
	}

	/**
	 *
	 * @Author 管正爽
	 * @Time 2016-7-28
	 * @备注 跳转添加舆情页面
	 */
	@RequiresPermissions("publicoption:publicOpinion:edit")
	@RequestMapping(value = "addPublicOpinion")
	public String addPublicOpinion(HttpServletRequest request, HttpServletResponse response){
		return "modules/publicoption/addPublicOpinion";
	}

	/**
	 *
	 * @Author 管正爽
	 * @Time 2016-7-28
	 * @备注 添加舆情
	 */
	@RequiresPermissions("publicoption:publicOpinion:edit")
	@RequestMapping(value = "addPublicOpinionForm")
	public String addPublicOpinionForm(PublicOpinion publicOpinion,HttpServletRequest request, HttpServletResponse response,Model model){
		if (!beanValidator(model, publicOpinion)){
			return form(publicOpinion, model);
		}
		publicOpinion.setReplyCount("0");
		publicOpinion.setTransCount("0");
		publicOpinion.setViewCount("0");
		publicOpinionService.publicOpinionInsert(publicOpinion);
		return "redirect:"+Global.getAdminPath()+"/publicoption/publicOpinion/list";
	}

}