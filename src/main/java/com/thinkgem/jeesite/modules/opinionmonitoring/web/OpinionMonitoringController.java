/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionmonitoring.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import com.thinkgem.jeesite.modules.keywords.entity.KeyWords;
import com.thinkgem.jeesite.modules.publicoption.entity.PublicOpinion;
import com.thinkgem.jeesite.modules.publicoption.service.PublicOpinionService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.json.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.config.Global;
import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.opinionmonitoring.entity.OpinionMonitoring;
import com.thinkgem.jeesite.modules.opinionmonitoring.service.OpinionMonitoringService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * 监控信息管理Controller
 * @author dingshuang
 * @version 2016-07-11
 */
@Controller
@RequestMapping(value = "${adminPath}/opinionmonitoring/opinionMonitoring")
public class OpinionMonitoringController extends BaseController {

	@Autowired
	private OpinionMonitoringService opinionMonitoringService;

	@Autowired
	private PublicOpinionService publicOpinionService;
	
	@ModelAttribute
	public OpinionMonitoring get(@RequestParam(required=false) String id) {
		OpinionMonitoring entity = null;

		if (StringUtils.isNotBlank(id)){
			entity = opinionMonitoringService.get(id);
		}
		if (entity == null){
			entity = new OpinionMonitoring();
		}
		return entity;
	}
	
	@RequiresPermissions("opinionmonitoring:opinionMonitoring:view")
	@RequestMapping(value = {"list", ""})
	public String list(OpinionMonitoring opinionMonitoring, Page<OpinionMonitoring> page, Model model) {
		page.setPageSize(5);
		page = opinionMonitoringService.findPage(page, opinionMonitoring);
		model.addAttribute("page", page);
		return "modules/opinionmonitoring/opinionMonitoringList";
	}

	@RequiresPermissions("opinionmonitoring:opinionMonitoring:view")
	@RequestMapping(value = "form")
	public String form(OpinionMonitoring opinionMonitoring, Model model) {
		model.addAttribute("opinionMonitoring", opinionMonitoring);
		return "modules/opinionmonitoring/opinionMonitoringForm";
	}

	@RequiresPermissions("opinionmonitoring:opinionMonitoring:edit")
	@RequestMapping(value = "save")
	public String save(OpinionMonitoring opinionMonitoring, Model model, RedirectAttributes redirectAttributes) {
		if (!beanValidator(model, opinionMonitoring)){
			return form(opinionMonitoring, model);
		}
		opinionMonitoringService.save(opinionMonitoring);
		addMessage(redirectAttributes, "保存监控信息管理成功");
		return "redirect:"+Global.getAdminPath()+"/opinionmonitoring/opinionMonitoring/?repage";
	}
	
	@RequiresPermissions("opinionmonitoring:opinionMonitoring:edit")
	@RequestMapping(value = "delete")
	public String delete(HttpServletRequest request,OpinionMonitoring opinionMonitoring, RedirectAttributes redirectAttributes) {
		opinionMonitoringService.delete(opinionMonitoring);
		addMessage(redirectAttributes, "撤销监控成功");
		return "redirect:"+Global.getAdminPath()+"/opinionmonitoring/opinionMonitoring/list?pageSize="+request.getParameter("pageSize")+"&pageNo="+request.getParameter("pageNo");
	}


	/**
	 * 批量删除预警舆情
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "toBatchDel")
	public void toBatchDel(HttpServletRequest request,HttpServletResponse response) throws IOException {
		String ids = request.getParameter("ids");
		PrintWriter out = response.getWriter();
		if(StringUtils.isNotBlank(ids)){
			String[] _ids = ids.split(";");
			if(ids!=null && ids.length()>0){
				for (String id : _ids){
					if(StringUtils.isNotBlank(id)){
						opinionMonitoringService.delete(new OpinionMonitoring(id));
					}
				}
			}
		}
		out.write(JSONObject.valueToString(1).toString());
	}

	/**
	 * 转办（不跨域）
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value = "toTrans")
	public void toTrans(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String id = request.getParameter("id");
		PrintWriter out = response.getWriter();
		//删除监控数据
		opinionMonitoringService.delete(new OpinionMonitoring(id));
		//获取监控数据对象，并赋属性值到舆情中
		OpinionMonitoring opinionMonitoring = opinionMonitoringService.get(new OpinionMonitoring(id));
		opinionMonitoring.setIsTrans("1");//标记已经转办过了
		opinionMonitoringService.save(opinionMonitoring);
		PublicOpinion publicOpinion = new PublicOpinion();
		BeanUtils.copyProperties(opinionMonitoring,publicOpinion);
		publicOpinion.setId(null);
		publicOpinion.setStatus("1");
		publicOpinion.setInvolveField(request.getParameter("involveField"));
		publicOpinion.setInvolveTownName(request.getParameter("involveTownName"));
		Object result  = publicOpinionService.publicOpinionInto(publicOpinion);
		out.write(JSONObject.valueToString(result).toString());
	}


	/**
	 *
	 * @Author 管正爽
	 * @Time 2016-7-26
	 * @备注 一般监控菜单栏
     */
	@RequiresPermissions("opinionmonitoring:opinionMonitoring:view")
	@RequestMapping(value = {"generalList", ""})
	public String generalList(OpinionMonitoring opinionMonitoring, Page<OpinionMonitoring> page, Model model,HttpServletRequest request) {
		page.setPageSize(5);
		page = opinionMonitoringService.findGeneralPage(page, opinionMonitoring);
		model.addAttribute("page", page);
		return "modules/opinionmonitoring/commonlyMonitoringList";
	}

	/**
	 *
	 * @Author 管正爽
	 * @Time 2016-7-27
	 * @备注 升级监控等级
	 */
	@RequiresPermissions("opinionmonitoring:opinionMonitoring:view")
	@RequestMapping(value = {"updateUpgradeMonitoring",""})
	public void updateUpgradeMonitoring(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String id = request.getParameter("id");
		PrintWriter out = response.getWriter();
		opinionMonitoringService.updateUpgradeMonitoring(id);
		out.write(JSONObject.valueToString(1).toString());
	}

	/**
	 *
	 * @Author 管正爽
	 * @Time 2016-7-27
	 * @备注 降级监控等级
	 */
	@RequiresPermissions("opinionmonitoring:opinionMonitoring:view")
	@RequestMapping(value = {"updateDowngradeMonitoring",""})
	public void updateDowngradeMonitoring(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String id = request.getParameter("id");
		PrintWriter out = response.getWriter();
		opinionMonitoringService.updateDowngradeMonitoring(id);
		out.write(JSONObject.valueToString(1).toString());
	}

}