/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.thinkgem.jeesite.modules.opinionmonitoring.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.opinionmonitoring.entity.OpinionMonitoring;
import com.thinkgem.jeesite.modules.opinionmonitoring.dao.OpinionMonitoringDao;

/**
 * 监控信息管理Service
 * @author dingshuang
 * @version 2016-07-11
 */
@Service
@Transactional(readOnly = true)
public class OpinionMonitoringService extends CrudService<OpinionMonitoringDao, OpinionMonitoring> {

	public OpinionMonitoring get(String id) {
		return super.get(id);
	}
	
	public List<OpinionMonitoring> findList(OpinionMonitoring opinionMonitoring) {
		return super.findList(opinionMonitoring);
	}
	
	public Page<OpinionMonitoring> findPage(Page<OpinionMonitoring> page, OpinionMonitoring opinionMonitoring) {
		return super.findPage(page, opinionMonitoring);
	}


	@Transactional(readOnly = false)
	public void save(OpinionMonitoring opinionMonitoring) {
		super.save(opinionMonitoring);
	}
	
	@Transactional(readOnly = false)
	public void delete(OpinionMonitoring opinionMonitoring) {
		super.delete(opinionMonitoring);
	}


	public Page<OpinionMonitoring> findGeneralPage (Page<OpinionMonitoring> page, OpinionMonitoring opinionMonitoring){
		opinionMonitoring.setPage(page);
		page.setList(dao.findGeneralPage(opinionMonitoring));
		return page;
	 }

	@Transactional(readOnly = false)
	public void updateUpgradeMonitoring(String id){
		dao.updateUpgradeMonitoring(id);
	}

	@Transactional(readOnly = false)
	public void updateDowngradeMonitoring(String id){
		dao.updateDowngradeMonitoring(id);
	}
}