package com.metadata.warningCalculate;

import com.metadata.warningCalculate.util.NumberUtil;
import com.thinkgem.jeesite.common.utils.CacheUtils;
import com.thinkgem.jeesite.modules.collectdata.dao.CollectDataDao;
import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import com.thinkgem.jeesite.modules.opinionWarning.entity.OpinionWarning;
import com.thinkgem.jeesite.modules.opinionWarning.service.OpinionWarningService;
import com.thinkgem.jeesite.modules.opinionmonitoring.entity.OpinionMonitoring;
import com.thinkgem.jeesite.modules.opinionmonitoring.service.OpinionMonitoringService;
import com.thinkgem.jeesite.modules.warningCondition.entity.WarningCondition;
import com.thinkgem.jeesite.modules.warningCondition.service.WarningConditionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WarningCalculateSvc {

    private static Logger logger = LoggerFactory.getLogger(WarningCalculateSvc.class);

    @Autowired
    private WarningConditionService warningConditionService;

    @Autowired
    private OpinionMonitoringService opinionMonitoringService;

    @Autowired
    private OpinionWarningService opinionWarningService;

    @Autowired
    private CollectDataDao collectDataDao;

    /**
     * 该方法用于计算监控数据形成预警
     */
    public void calculate() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000 * 10 * 1);
                        //TODO 后期应该改写当前程序，使用quartz进行定时任务调度
                        Object isIndexOver = CacheUtils.get("CALCULATE_WARNING", "isCalculateOver");
                        if (isIndexOver == null || (Boolean.valueOf(isIndexOver.toString()))) {
                            logger.info("--------->重新开始一次对监控数据的预警分析计算……");
                            calculateMonitoringToWarning();
                            logger.info("--------->已经完成一次对监控数据的预警分析计算……");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        thread.start();
    }


    /**
     * 计算监控数据，形成预警数据
     */
    private void calculateMonitoringToWarning() {
        CacheUtils.put("CALCULATE_WARNING", "isCalculateOver", false);
        List<WarningCondition> list = warningConditionService.findList(new WarningCondition());
        if (list != null && list.size() > 0) {
            List<OpinionMonitoring> monitorings = opinionMonitoringService.findList(new OpinionMonitoring());
            for (WarningCondition warningCondition : list) {
                if ("1".equals(warningCondition.getIsUsed())) {
                    if ("viewCount".equals(warningCondition.getFunc())) {
                        String params[] = warningCondition.getParam().split(";");
                        //访问量超过限定值
                        countBeyondLimit(params[0], monitorings, "viewCount");
                    } else if ("replyCount".equals(warningCondition.getFunc())) {
                        String params[] = warningCondition.getParam().split(";");
                        //回复量超过限定值
                        countBeyondLimit(params[0], monitorings, "replyCount");
                    } else if ("transCount".equals(warningCondition.getFunc())) {
                        String params[] = warningCondition.getParam().split(";");
                        //浏览超过限定值
                        countBeyondLimit(params[0], monitorings, "transCount");
                    } else if ("viewCountIncre".equals(warningCondition.getFunc())) {
                        //浏览增量超过限定值
                        String params[] = warningCondition.getParam().split(";");
                        countIncreBeyondLimit(params[0], params[1], monitorings, "viewCountIncre");
                    } else if ("replyCountIncre".equals(warningCondition.getFunc())) {
                        //回复增量超过限定值
                        String params[] = warningCondition.getParam().split(";");
                        countIncreBeyondLimit(params[0], params[1], monitorings, "replyCountIncre");
                    } else if ("transCountIncre".equals(warningCondition.getFunc())) {
                        //转发增量超过限定值
                        String params[] = warningCondition.getParam().split(";");
                        countIncreBeyondLimit(params[0], params[1], monitorings, "transCountIncre");
                    }
                }
            }
        }
        CacheUtils.put("CALCULATE_WARNING", "isCalculateOver", true);
    }


    /**
     * 总量超过限定
     *
     * @param limitValue
     * @param monitorings
     */
    private void countBeyondLimit(String limitValue, List<OpinionMonitoring> monitorings, String func) {
        if (monitorings != null && monitorings.size() > 0) {
            for (OpinionMonitoring monitoring : monitorings) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("origin_url", monitoring.getOriginUrl());
                map.put("func", "transCount");
                List<CollectData> collectDatas = collectDataDao.getCollectDataByURL(map);
                if (collectDatas != null && collectDatas.size() > 0) {
                    for (CollectData collectData : collectDatas) {
                        if ("viewCount".equals(func)) {
                            int viewCount = Integer.parseInt(NumberUtil.updateExceptionNum(collectData.getViewCount()));
                            if (viewCount > Integer.parseInt(limitValue)) {
                                String warningReason = "转发总量超过了<font style='color:red;'>" + limitValue + "</font>条";
                                this.infoOpinionWarning(collectData, new OpinionWarning(), warningReason);
                            }
                        }
                        if ("replyCount".equals(func)) {
                            int replyCount = Integer.parseInt(NumberUtil.updateExceptionNum(collectData.getReplyCount()));
                            if (replyCount > Integer.parseInt(limitValue)) {
                                String warningReason = "回复总量超过了<font style='color:red;'>" + limitValue + "</font>条";
                                this.infoOpinionWarning(collectData, new OpinionWarning(), warningReason);
                            }
                        }
                        if ("transCount".equals(func)) {
                            int transCount = Integer.parseInt(NumberUtil.updateExceptionNum(collectData.getTransCount()));
                            if (transCount > Integer.parseInt(limitValue)) {
                                String warningReason = "转发总量超过了<font style='color:red;'>" + limitValue + "</font>条";
                                this.infoOpinionWarning(collectData, new OpinionWarning(), warningReason);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * 浏览量增量超过限定
     *
     * @param duration
     * @param limitValue
     * @param monitorings
     */
    private void countIncreBeyondLimit(String duration, String limitValue, List<OpinionMonitoring> monitorings, String func) {
        if (monitorings != null && monitorings.size() > 0) {
            for (OpinionMonitoring monitoring : monitorings) {
                //获取最新一次采集的采集数据
                CollectData newCollectData = collectDataDao.getCollectDataNewCollect(monitoring.getOriginUrl());
                if (newCollectData != null) {
                    //获取时间间隔前最新的那一条数据D
                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("origin_url", monitoring.getOriginUrl());
                    map.put("id", newCollectData.getId());
                    map.put("baseTime", newCollectData.getCollectDate());
                    map.put("duration", duration);
                    CollectData durationNewcollectData = collectDataDao.getCollectDataDurationNewCollect(map);
                    if (durationNewcollectData != null) {
                        if ("viewCountIncre".equals(func)) {
                            int newDateViewCount = Integer.parseInt(NumberUtil.updateExceptionNum(newCollectData.getViewCount()));
                            int durationNewDateViewCount = Integer.parseInt(NumberUtil.updateExceptionNum(durationNewcollectData.getViewCount()));
                            if (newDateViewCount - durationNewDateViewCount > Integer.parseInt(limitValue)) {
                                String warningReason = "<font style='color:red;'>" + duration + "</font>小时内,浏览增量超过了<font style='color:red;'>" + limitValue + "</font>条";
                                this.infoOpinionWarning(newCollectData, new OpinionWarning(), warningReason);
                            }
                        }
                        if ("replyCountIncre".equals(func)) {
                            int newDateReplyCount = Integer.parseInt(NumberUtil.updateExceptionNum(newCollectData.getReplyCount()));
                            int durationNewDateReplyCount = Integer.parseInt(NumberUtil.updateExceptionNum(durationNewcollectData.getReplyCount()));
                            if (newDateReplyCount - durationNewDateReplyCount > Integer.parseInt(limitValue)) {
                                String warningReason = "<font style='color:red;'>" + duration + "</font>小时内,回复增量超过了<font style='color:red;'>" + limitValue + "</font>条";
                                this.infoOpinionWarning(newCollectData, new OpinionWarning(), warningReason);
                            }
                        }
                        if ("transCountIncre".equals(func)) {
                            int newDateTransCount = Integer.parseInt(NumberUtil.updateExceptionNum(newCollectData.getTransCount()));
                            int durationNewDateTransCount = Integer.parseInt(NumberUtil.updateExceptionNum(durationNewcollectData.getTransCount()));
                            if (newDateTransCount - durationNewDateTransCount > Integer.parseInt(limitValue)) {
                                String warningReason = "<font style='color:red;'>" + duration + "</font>小时内,转发增量超过了<font style='color:red;'>" + limitValue + "</font>条";
                                this.infoOpinionWarning(newCollectData, new OpinionWarning(), warningReason);
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * 该方法用于初始化一条预警数据
     *
     * @param collectData
     * @param opinionWarning
     * @param warningReason
     */
    private void infoOpinionWarning(CollectData collectData, OpinionWarning opinionWarning, String warningReason) {
        int count = opinionWarningService.getOpinionWarningCount(collectData.getOriginUrl());
        if (!(count > 0)) {
            //避免重复插入
            BeanUtils.copyProperties(collectData, opinionWarning);
            opinionWarning.setId(null);
            opinionWarning.setIsTrans("0");
            opinionWarning.setDelFlag("0");
            opinionWarning.setWarningReason(warningReason);
            opinionWarningService.save(opinionWarning);
        }
    }

}
