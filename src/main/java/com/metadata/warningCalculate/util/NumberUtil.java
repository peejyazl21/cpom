/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.metadata.warningCalculate.util;

import com.thinkgem.jeesite.common.utils.StringUtils;

/**
 * 该方法类用于处理数据
 *
 * @author dingshuang
 * @version 2016-06-21
 */
public class NumberUtil {

    /**
     * 该方法用于处理异常数据
     * @param num
     * @return
     */
    public static String updateExceptionNum(String num){
        String result="0";
        if(StringUtils.isNotBlank(num)){
            num = num.replaceAll(",","").replaceAll("，","");//防止有","出现
            if(num.contains("K") || num.contains("k")){
                String mun =  num.replaceAll(num,"K").replaceAll(num,"k");
                result = String.valueOf(Double.parseDouble(mun.trim())*1000);
            }else if(num.contains("万")){
                String mun =  num.replaceAll(num,"万");
                result = String.valueOf(Double.parseDouble(mun.trim())*10000);
            }else if(num.contains("百万")){
                String mun =  num.replaceAll(num,"百万");
                result = String.valueOf(Double.parseDouble(mun.trim())*100000);
            }else if(num.contains("千万")){
                String mun =  num.replaceAll(num,"千万");
                result = String.valueOf(Double.parseDouble(mun.trim())*1000000);
            }else if(num.contains("亿")){
                String mun =  num.replaceAll(num,"亿");
                result = String.valueOf(Double.parseDouble(mun.trim())*10000000);
            }else{
                result = num;
            }
        }
        return result;
    }

    public static void main(String[] str){
        System.out.println(NumberUtil.updateExceptionNum("1.5K"));
        System.out.println(NumberUtil.updateExceptionNum("1.3k"));
        System.out.println(NumberUtil.updateExceptionNum("1.5 万"));
        System.out.println(NumberUtil.updateExceptionNum("1 百万"));
        System.out.println(NumberUtil.updateExceptionNum("1.45 千万"));
        System.out.println(NumberUtil.updateExceptionNum("1.6 亿"));
    }


}