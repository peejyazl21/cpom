package com.metadata.lucene;

import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.modules.collectdata.entity.CollectData;
import com.thinkgem.jeesite.modules.keywords.entity.KeyWords;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.DateTools;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryParser.MultiFieldQueryParser;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.search.*;
import org.apache.lucene.util.Version;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * collectdata的lucence管理类
 * author  dingshuang
 */
public class LuceneCollectData {

    public static final String ID = "id";
    public static final String TITLE = "title";        // 标题
    public static final String CONTENT = "content";        // 内容
    public static final String RELEASE_TIME = "releaseTime"; //发布时间
    public static final String[] QUERY_FIELD = {TITLE, CONTENT};
    public static final BooleanClause.Occur[] QUERY_FLAGS = {BooleanClause.Occur.SHOULD, BooleanClause.Occur.SHOULD};


    /**
     * 创建索引文档
     *
     * @param collectData
     * @return
     */
    public static Document createDocument(CollectData collectData) {
        Document doc = new Document();
        doc.add(new Field(ID, collectData.getId().toString(), Field.Store.YES,
                Field.Index.NOT_ANALYZED));
        doc.add(new Field(TITLE, collectData.getTitle().toString(), Field.Store.NO,
                Field.Index.ANALYZED));
        doc.add(new Field(CONTENT, collectData.getContent().toString(), Field.Store.NO,
                Field.Index.ANALYZED));
        doc.add(new Field(RELEASE_TIME, collectData.getReleaseTime().toString(), Field.Store.NO,
                Field.Index.NOT_ANALYZED));
        return doc;
    }

    /**
     * 创建查询
     *
     * @param stringQuerys
     * @param startDate
     * @param endDate
     * @param analyzer
     * @return
     * @throws ParseException
     */
    public static Query createQuery(List<KeyWords> stringQuerys, Date startDate, Date endDate, Analyzer analyzer)
            throws ParseException {
        BooleanQuery bq = new BooleanQuery();
        if (stringQuerys != null && stringQuerys.size() > 0) {
            for (KeyWords keyWord : stringQuerys) {
                String word = keyWord.getWord();
                if (StringUtils.isNotBlank(word)) {
                    bq.add(MultiFieldQueryParser.parse(Version.LUCENE_30, word, QUERY_FIELD, QUERY_FLAGS, analyzer), BooleanClause.Occur.SHOULD);
                }
            }
        }
        if (startDate != null || endDate != null) {
            String start = null;
            String end = null;
            if (startDate != null) {
                start = DateTools.dateToString(startDate, DateTools.Resolution.DAY);
            }
            if (endDate != null) {
                end = DateTools.dateToString(endDate, DateTools.Resolution.DAY);
            }
            bq.add(new TermRangeQuery(RELEASE_TIME, start, end, true, true), BooleanClause.Occur.MUST);
        }
        return bq;
    }

    /**
     * 按照发布时间段删除索引
     *
     * @param startDate
     * @param endDate
     * @param writer
     * @throws IOException
     * @throws ParseException
     */
    public static void delete(Date startDate, Date endDate, IndexWriter writer)
            throws IOException, ParseException {
        writer.deleteDocuments(createQuery(null, startDate, endDate, null));
    }


    /**
     * 根据id删除索引
     *
     * @param id
     * @param writer
     * @throws IOException
     * @throws ParseException
     */
    public static void delete(String id, IndexWriter writer)
            throws IOException, ParseException {
        writer.deleteDocuments(new Term(ID, id));
    }

    /**
     * 获取检索分页
     *
     * @param searcher
     * @param docs
     * @param params   [0]查询总量[1]查询开始点
     * @return
     * @throws IOException
     */
    public static List<String> getResultPage(Searcher searcher, TopDocs docs, int[] params) throws IOException {
        List<String> list = new ArrayList<String>();
        ScoreDoc[] hits = docs.scoreDocs;
        for (int i = params[1]; i < params[0]; i++) {
            Document d = searcher.doc(hits[i].doc);
            list.add(d.getField(ID).stringValue());
        }
        Collections.reverse(list);
        return list;
    }

}