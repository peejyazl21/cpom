<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>舆情监控管理</title>
	<meta name="decorator" content="default"/>
</head>
<body>
<ul class="nav nav-tabs">
	<li class="active"><a href="${ctx}/publicoption/publicOpinion/">预警条件设置</a></li>
</ul>
<table id="contentTable" class="table table-striped table-bordered table-condensed">
	<thead>
	<tr>
		<th style="width: 20px;">序号</th>
		<th>预警条件</th>
		<th>是否启用</th>
	</tr>
	</thead>
	<tbody id="warningParamTbody">
		<tr>
			<td>1
				<input type="hidden" name="func" value="viewCount"/>
				<input type="hidden" name="id"/>
			</td>
			<td>浏览总量超过<input name="params" style="width: 30px; margin: 0px 3px 0px 3px ;"/>条</td>
			<td>
				是<input type="radio" value="1" name="isUsed_1"  checked="checked"/>&nbsp;&nbsp;
				否<input type="radio" value="0" name="isUsed_1" />
			</td>
		</tr>
		<tr>
			<td>2
				<input type="hidden" name="func" value="replyCount"/>
				<input type="hidden" name="id"/>
			</td>
			<td>回复总量超过<input name="params"  style="width: 30px; margin: 0px 3px 0px 3px ;"/>条</td>
			<td>
				是<input type="radio" value="1" name="isUsed_2"  checked="checked"/>&nbsp;&nbsp;
				否<input type="radio" value="0" name="isUsed_2"/>
			</td>
		</tr>
		<tr>
			<td>3
				<input type="hidden" name="func" value="transCount"/>
				<input type="hidden" name="id"/>
			</td>
			<td>转载总量超过<input name="params" style="width: 30px; margin: 0px 3px 0px 3px ;"/>条</td>
			<td>
				是<input type="radio" value="1" name="isUsed_3"  checked="checked"/>&nbsp;&nbsp;
				否<input type="radio" value="0" name="isUsed_3"/>
			</td>
		</tr>
		<tr>
			<td>4
				<input type="hidden" name="func" value="viewCountIncre"/>
				<input type="hidden" name="id"/>
			</td>
			<td><input name="params_1" style="width: 30px; margin: 0px 3px 0px 3px ;"/>小时内，浏览量增长超过
				<input name="params_2"  style="width: 30px; margin: 0px 3px 0px 3px ;"/>条
			</td>
			<td>
				是<input type="radio" value="1" name="isUsed_4" checked="checked"/>&nbsp;&nbsp;
				否<input type="radio" value="0" name="isUsed_4" />
			</td>
		</tr>
		<tr>
			<td>5
				<input type="hidden" name="func" value="replyCountIncre"/>
				<input type="hidden" name="id"/>
			</td>
			<td><input name="params_1" style="width: 30px; margin: 0px 3px 0px 3px ;"/>小时内，回复量增长超过
				<input name="params_2"  style="width: 30px; margin: 0px 3px 0px 3px ;"/>条
			</td>
			<td>
				是<input type="radio" value="1" name="isUsed_5" checked="checked"/>&nbsp;&nbsp;
				否<input type="radio" value="0" name="isUsed_5"/>
			</td>
		</tr>
		<tr>
			<td>6
				<input type="hidden" name="func" value="transCountIncre"/>
				<input type="hidden" name="id"/>
			</td>
			<td><input name="params_1" style="width: 30px; margin: 0px 3px 0px 3px ;"/>小时内，转载量增长超过
				<input name="params_2"  style="width: 30px; margin: 0px 3px 0px 3px ;"/>条
			</td>
			<td>
				是<input type="radio" value="1" name="isUsed_6" checked="checked"/>&nbsp;&nbsp;
				否<input type="radio" value="0" name="isUsed_6" />
			</td>
		</tr>
		<tr>
			<td colspan="3" style="text-align: center;">
				<input name="btnSubmit" class="btn btn-primary" type="button" value="保存"/>
			</td>
		</tr>
	</tbody>
</table>
<div class="pagination">${page}</div>
<script type="text/javascript">
	$(document).ready(function() {
		//回显预警条件
		$.ajax({
			type:"post",
			url : "${ctx}/warningCondition/warningCondition/jsonData",
			dataType:"json",
			success:function(jsonDatas){
				$("#warningParamTbody tr").not(":last").each(function(i){
					var jsonData = jsonDatas[i];
					var id = $(this).find("input[name='id']").val(jsonData['id']);
					if(jsonData['isUsed'] == '0'){
						$(this).find("input[name^='isUsed']:first-child").attr("checked",false);
						$(this).find("input[name^='isUsed']:last-child").attr("checked","checked");
					}else if(jsonData['isUsed'] == '1'){
						$(this).find("input[name^='isUsed']:first-child").attr("checked","checked");
						$(this).find("input[name^='isUsed']:last-child").attr("checked",false);
					}
					var params = jsonData['param'].split(";");
					$(this).find("input[name^='params']").each(function(i){
						 $(this).val(params[i]);
					});
				});
			}
		});
		//保存预警条件
		$("input[name='btnSubmit']").bind("click",function(){
			var warningParams ="";
			$("#warningParamTbody tr").not(":last").each(function(){
				var id = $(this).find("input[name='id']").val();
				var func = $(this).find("input[name='func']").val();
				var isUsed = $(this).find("input[name^='isUsed']:checked").val();
                var params ="";
				$(this).find("input[name^='params']").each(function(){
					params+=$(this).val()+";";
				});
				var warningParam = id+"_"+func+"_"+isUsed+"_"+params ;
				warningParams+="-"+warningParam;
			});
			$.ajax({
				type:"post",
				url : "${ctx}/warningCondition/warningCondition/save",
				dataType:"json",
				data:{warningParams:warningParams},
				success:function(result){
					if(result==1){
						$.jBox.tip('预警条件已保存！');
						location.href="${ctx}/warningCondition/warningCondition/list";
					}
				}
			});

		});
	});
</script>
</body>
</html>