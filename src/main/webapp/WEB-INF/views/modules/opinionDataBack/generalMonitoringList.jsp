<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>舆情监控管理</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function() {
        });


        function page(n,s){
            $("#pageNo").val(n);
            $("#pageSize").val(s);
            $("#searchForm").submit();
            return false;
        }

        /**
         * 转办操作
         * @param id
         */
        function toTrans(id){
            var html = "<div style='padding:10px;'>涉及领域：<select id='involveField' name='involveField'>";
            html+=	"<option value=''>请选择</option>";
            html+=	"<option value='7'>农民土地</option>";
            html+=	"<option value='6'>城管环卫</option>";
            html+=	"<option value='5'>教育教学</option>";
            html+=	"<option value='4'>房产物业</option>";
            html+=	"<option value='3'>环境问题</option>";
            html+=	"<option value='2'>公安消防</option>";
            html+=	"<option value='1'>公共交通</option>";
            html+=	"</select></div>";
            html+=	"<div style='padding:10px;'>涉及街道：<input type='text' id='involveTownName' name='involveTownName' /></div>";
            var submit = function (v, h, f) {
                if (f.involveField == '') {
                    $.jBox.tip("请输入涉及领域", 'error', { focusId: "involveField" });
                    return false;
                }
                if (f.involveTownName == '') {
                    $.jBox.tip("请输入涉及街道", 'error', { focusId: "involveTownName" });
                    return false;
                }
                //提交办理请求
                $.ajax({
                    type:"GET",
                    url : "${ctx}/opinionJudge/opinionJudge/toTrans",
                    dataType:"json",
                    data:{id:id,
                        involveField:f.involveField,
                        involveTownName:f.involveTownName},
                    success:function(result){
                        if(result==1){
                            $.jBox.tip('已发送舆情办理任务至信息中心！');
                            $("#a_"+id).html("已办");
                            location.href="${ctx}/opinionDataBack/opinionDataBack/list?pageSize=${page.pageSize}&pageNo=${page.pageNo}";
                        }
                    }
                });

            };
            $.jBox(html, { title: "完善舆情信息", submit: submit });
        }


        /**
         * 该方法用于信息监控
         */
        function toMonitoring(id){
            $.jBox.tip('监控添加中,请稍候！');
            $.ajax({
                type:"GET",
                url : "${ctx}/opinionJudge/opinionJudge/toMonitoring",
                dataType:"json",
                data:{id:id},
                success:function(result){
                    if(result==1){
                        location.href="${ctx}/opinionDataBack/opinionDataBack/list?pageSize=${page.pageSize}&pageNo=${page.pageNo}";
                    }
                }
            });
        }


        /**
         * 该方法用于恢复数据
         */
        function toDataBack(id){
            $.jBox.tip('数据恢复中,请稍候！');
            $.ajax({
                type:"GET",
                url : "${ctx}/opinionDataBack/opinionDataBack/dataBack",
                dataType:"json",
                data:{id:id,dataTag:1},
                success:function(result){
                    if(result==1){
                        location.href="${ctx}/opinionDataBack/opinionDataBack/list?pageSize=${page.pageSize}&pageNo=${page.pageNo}";
                    }
                }
            });
        }


    </script>
</head>
<body>
<ul class="nav nav-tabs">
    <li ><a href="${ctx}/opinionDataBack/opinionDataBack/list">研判回收列表</a></li>
<%--
    <li><a href="${ctx}/opinionDataBack/opinionDataBack/monitoringList/">监控回收列表</a></li>
--%>
    <li><a href="${ctx}/opinionDataBack/opinionDataBack/warningList/">预警回收列表</a></li>
    <li><a href="${ctx}/opinionDataBack/opinionDataBack/opinionMonitoringList">重点监控回收</a></li>
    <li class="active"><a href="${ctx}/opinionDataBack/opinionDataBack/generalMonitoringList">一般监控回收</a></li>
</ul>
<form:form id="searchForm" action="${ctx}/opinionDataBack/opinionDataBack/generalMonitoringList" method="post" class="breadcrumb form-search">
    <input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
    <input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
    <ul class="ul-form">
        <li><input name="title" value="${title}"/></li>
        <li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
        <li class="clearfix"></li>
    </ul>
</form:form>
<sys:message content="${message}"/>
<table id="contentTable" class="table table-striped table-bordered table-condensed">
    <thead>
    <tr>
        <th style="width: 50%;">标题及内容</th>
        <th>来源</th>
        <th>发布时间</th>
        <th>浏览量</th>
        <th>转发量</th>
        <th>回复量</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <c:if test="${empty page.list}">
        <tr>
            <td colspan="7">没有需要研判的数据</td>
        </tr>
    </c:if>
    <c:forEach items="${page.list}" var="collectData">
        <tr>
            <td>
                <h6><span title="${collectData.content}">${collectData.title}</span></h6>
                <br/>
					<span><div style="width:500px; white-space:nowrap; text-overflow:ellipsis; -o-text-overflow:ellipsis; overflow: hidden;">
                        <xmp></xmp>${collectData.content}
                    </div></span>
            </td>
            <td><a href="${collectData.originUrl}" target="_blank">${collectData.collectOrigin}</a></td>
            <td>${collectData.releaseTime}</td>
            <td>${collectData.viewCount}</td>
            <td>${collectData.transCount}</td>
            <td>${collectData.replyCount}</td>
            <td>
                <a id="a_${collectData.id}" href="javascript:toTrans('${collectData.id}');">
                    <c:if test="${collectData.isTrans eq '1'}">
                        已办
                    </c:if>
                    <c:if test="${collectData.isTrans !='1'}">
                        办理
                    </c:if>
                </a>
                <br/>
                <a id="a_${collectData.id}" href="javascript:toDataBack('${collectData.id}');">恢复</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
<div class="pagination">${page}</div>
</body>
</html>