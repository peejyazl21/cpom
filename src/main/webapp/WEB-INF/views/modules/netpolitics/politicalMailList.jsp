<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>网络问政管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/netpolitics/politicalMail/">网络问政列表</a></li>
		<shiro:hasPermission name="netpolitics:politicalMail:edit"><li><a href="${ctx}/netpolitics/politicalMail/form">网络问政添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="politicalMail" action="${ctx}/netpolitics/politicalMail/" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>来信主题：</label>
				<form:input path="subject" htmlEscape="false" maxlength="500" class="input-medium"/>
			</li>
			<li><label>姓名：</label>
				<form:input path="name" htmlEscape="false" maxlength="100" class="input-medium"/>
			</li>
			<li><label>来访目的：</label>
				<form:select path="aproval" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mail_aproval')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>问题分类：</label>
				<form:select path="classify" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mail_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>是否处理：</label>
				<form:select path="state" class="input-medium">
					<form:option value="" label=""/>
					<form:options items="${fns:getDictList('mail_state')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
				</form:select>
			</li>
			<li><label>提交时间：</label>
				<input name="beginApplyTiime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${politicalMail.beginApplyTiime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endApplyTiime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${politicalMail.endApplyTiime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li><label>办理时间：</label>
				<input name="beginHandleTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${politicalMail.beginHandleTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/> - 
				<input name="endHandleTime" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate"
					value="<fmt:formatDate value="${politicalMail.endHandleTime}" pattern="yyyy-MM-dd HH:mm:ss"/>"
					onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',isShowClear:false});"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>来信主题</th>
				<th>姓名</th>
				<th>联系电话</th>
				<th>来访目的</th>
				<th>问题分类</th>
				<th>是否公开</th>
				<th>是否处理</th>
				<th>提交时间</th>
				<th>受理部门</th>
				<shiro:hasPermission name="netpolitics:politicalMail:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="politicalMail">
			<tr>
				<td><a href="${ctx}/netpolitics/politicalMail/form?id=${politicalMail.id}">
					${politicalMail.subject}
				</a></td>
				<td>
					${politicalMail.name}
				</td>
				<td>
					${politicalMail.mobile}
				</td>
				<td>
					${fns:getDictLabel(politicalMail.aproval, 'mail_aproval', '')}
				</td>
				<td>
					${fns:getDictLabel(politicalMail.classify, 'mail_type', '')}
				</td>
				<td>
					${fns:getDictLabel(politicalMail.open, 'mail_open', '')}
				</td>
				<td>
					${fns:getDictLabel(politicalMail.state, 'mail_state', '')}
				</td>
				<td>
					<fmt:formatDate value="${politicalMail.applyTiime}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>
				<td>
					${politicalMail.hdept}
				</td>
				<shiro:hasPermission name="netpolitics:politicalMail:edit"><td>
    				<a href="${ctx}/netpolitics/politicalMail/form?id=${politicalMail.id}">修改</a>
					<a href="${ctx}/netpolitics/politicalMail/delete?id=${politicalMail.id}" onclick="return confirmx('确认要删除该网络问政吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>