<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>监控信息管理管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//全选或者不选
			$("input[type='checkbox'][name='id_']").click(function() {
				$('input[name^="id_"]').attr("checked",this.checked);
			});
			$("#btnBatchDel").bind("click",function(){
				var ids="";
				$("input[name^='id_']").each(function() {
					if ($(this).attr("checked")) {
						ids += ";"+$(this).val();
					}
				});
				if(ids != null && ids!=''){
					$.ajax({
						type:"GET",
						url : "${ctx}/opinionmonitoring/opinionMonitoring/toBatchDel",
						dataType:"json",
						data:{ids:ids},
						success:function(){
							top.$.jBox.tip('批量撤控完成');
							location.href="${ctx}/opinionmonitoring/opinionMonitoring/list?pageSize=${page.pageSize}&pageNo=${page.pageNo}";
						}
					});
				}else{
					top.$.jBox.tip('对不起，撤控失败，请联系管理人员');
				}
			});
		});


		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }


		/**
		 * 转办操作
		 * @param id
		 */
		function toTrans(id){
			var html = "<div style='padding:10px;'>涉及领域：<select id='involveField' name='involveField'>";
			html+=	"<option value=''>请选择</option>";
			html+=	"<option value='7'>农民土地</option>";
			html+=	"<option value='6'>城管环卫</option>";
			html+=	"<option value='5'>教育教学</option>";
			html+=	"<option value='4'>房产物业</option>";
			html+=	"<option value='3'>环境问题</option>";
			html+=	"<option value='2'>公安消防</option>";
			html+=	"<option value='1'>公共交通</option>";
			html+=	"</select></div>";
			html+=	"<div style='padding:10px;'>涉及街道：<input type='text' id='involveTownName' name='involveTownName' /></div>";
			var submit = function (v, h, f) {
				if (f.involveField == '') {
					$.jBox.tip("请输入涉及领域", 'error', { focusId: "involveField" });
					return false;
				}
				if (f.involveTownName == '') {
					$.jBox.tip("请输入涉及街道", 'error', { focusId: "involveTownName" });
					return false;
				}
				//提交办理请求
				$.ajax({
					type:"GET",
					url : "${ctx}/opinionmonitoring/opinionMonitoring/toTrans",
					dataType:"json",
					data:{id:id,
						involveField:f.involveField,
						involveTownName:f.involveTownName},
					success:function(result){
						if(result==1){
							$.jBox.tip('已发送舆情办理任务至信息中心！');
							$("#a_"+id).html("已办");
							location.href="${ctx}/opinionmonitoring/opinionMonitoring/list?pageSize=${page.pageSize}&pageNo=${page.pageNo}";
						}
					}
				});

			};
			$.jBox(html, { title: "完善舆情信息", submit: submit });
		}

		//处理降低监控等级
		function downgradeMonitoring(id){
			var html = "<div style='padding:10px;'>确认要将该监控信息等级降低吗?</div>";
			var submit = function (v, h, f) {
				$.ajax({
					type:"GET",
					url : "${ctx}/opinionmonitoring/opinionMonitoring/updateDowngradeMonitoring",
					dataType:"json",
					data:{id:id},
					success:function(result){
						if(result==1){
							location.href="${ctx}/opinionmonitoring/opinionMonitoring/list?pageSize=${page.pageSize}&pageNo=${page.pageNo}";
						}
					}
				});
			};
			$.jBox(html, { title: "更改监控信息", submit: submit });
		}

	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/opinionmonitoring/opinionMonitoring/">监控信息管理列表</a></li>
		<%--<shiro:hasPermission name="opinionmonitoring:opinionMonitoring:edit"><li><a href="${ctx}/opinionmonitoring/opinionMonitoring/form">监控信息管理添加</a></li></shiro:hasPermission>--%>
	</ul>
	<form:form id="searchForm" modelAttribute="opinionMonitoring" action="${ctx}/opinionmonitoring/opinionMonitoring/list" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>标题：</label>
				<form:input path="title" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="btns"><input id="btnBatchDel" class="btn btn-primary" type="button" value="批量撤销"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
		<tr>
			<th><input type="checkbox" name="id_"></th>
			<th style="width: 50%;">标题及内容</th>
			<th>来源</th>
			<th>发布时间</th>
			<th>浏览量</th>
			<th>转发量</th>
			<th>回复量</th>
			<shiro:hasPermission name="opinionmonitoring:opinionMonitoring:edit"><th>操作</th></shiro:hasPermission>
		</tr>
		</thead>
		<tbody>
		<c:if test="${empty page.list}">
			<tr>
				<td colspan="8">没有监控的数据</td>
			</tr>
		</c:if>
		<c:forEach items="${page.list}" var="opinionMonitoring">
			<tr>
				<th><input type="checkbox"  name="id_${opinionMonitoring.id}" value="${opinionMonitoring.id}"></th>
				<td>
					<h6><span title="${opinionMonitoring.content}">${opinionMonitoring.title}</span></h6>
					<br/>
					<span><div style="width:500px; white-space:nowrap; text-overflow:ellipsis; -o-text-overflow:ellipsis; overflow: hidden;">
						<xmp></xmp>${opinionMonitoring.content}
					</div></span>
				</td>
				<td><a href="${opinionMonitoring.originUrl}" target="_blank">${opinionMonitoring.collectOrigin}</a></td>
				<td>${opinionMonitoring.releaseTime}</td>
				<td>${opinionMonitoring.viewCount}</td>
				<td>${opinionMonitoring.transCount}</td>
				<td>${opinionMonitoring.replyCount}</td>
				<td>
					<shiro:hasPermission name="opinionmonitoring:opinionMonitoring:edit">
						<a id="a_${opinionMonitoring.id}" href="javascript:toTrans('${opinionMonitoring.id}',this);">
							<c:if test="${opinionMonitoring.isTrans eq '1'}">
								已办
							</c:if>
							<c:if test="${opinionMonitoring.isTrans !='1'}">
								办理
							</c:if>
						</a>
						<br/>
						<a href="${ctx}/opinionmonitoring/opinionMonitoring/delete?id=${opinionMonitoring.id}&pageSize=${page.pageSize}&pageNo=${page.pageNo}" onclick="return confirmx('确认要删除该监控信息管理吗？', this.href)">撤控</a>
						<br>
						<a href="javascript:downgradeMonitoring('${opinionMonitoring.id}',this)"  title="降为一般监控">降级</a>
					</shiro:hasPermission>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>