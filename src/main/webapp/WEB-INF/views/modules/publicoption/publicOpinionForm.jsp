<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>舆情监控管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			//信息中心分发操作
			$("#hand_out").bind("click",function(){
				var _$office_id = $("#inputForm input[name='office.id']");
				if(_$office_id.val()!="" && _$office_id.val()!=null){
					loading('正在提交，请稍等...');
					$("#inputForm").submit();
				}else{
					top.$.jBox.tip('请选择分发机构！');
				}
			});

			//办理部门的办理动作
			$("#inputForm input[name='next_action']").each(function(i){
				$(this).bind("click",function(){
					if(i==0){
						//办结
						$("tr[name^='deal_with_1_tr']").add("#deal_with_1").show();
						$("tr[name^='deal_with_2_tr']").add("#deal_with_2").hide();
						$("#deal_with_1").bind("click",function(){
							if (CKEDITOR.instances.replyContent.getData()=="") {
								top.$.jBox.tip('请填写回复内容', 'warning');
								return;
							}
							var _$replyProve = $("#inputForm input[name='replyProve']");
							if(_$replyProve.val()=="" || _$replyProve.val()==null){
								top.$.jBox.tip('请上传回复证明', 'warning');
								return;
							}
							loading('正在提交，请稍等...');
							$("#inputForm input[name='action']").val("deal_with_1");
							$("#inputForm").submit();
						});
					}else if(i==1){
						//申请协办
						$("tr[name^='deal_with_2_tr']").add("#deal_with_2").show();
						$("tr[name^='deal_with_1_tr']").add("#deal_with_1").hide();
                        //绑定协办事件
						$("#deal_with_2").bind("click",function(){
							var _$office_id = $("#inputForm input[name='office.id']");
							if(_$office_id.val()!="" && _$office_id.val()!=null){
								loading('正在提交，请稍等...');
								//修改动作信息，表明是要求协办
								$("#inputForm input[name='action']").val("deal_with_2");
								$("#inputForm").submit();
							}else{
								top.$.jBox.tip('请选择协办机构！');
							}
						});
					}else if(i==2){
						//退回
						loading('正在提交，请稍等...');
						//修改动作信息，表明是要求协办
						$("#inputForm input[name='action']").val("deal_with_0");
						$("#inputForm").submit();
					}
				});
			});

			//完成协办
			$("#join_hands").bind("click",function(){
				if (CKEDITOR.instances.synergizeContent.getData()=="") {
					top.$.jBox.tip('请填写协办内容', 'warning');
				}else{
					loading('正在提交，请稍等...');
					$("#inputForm").submit();
				}
			});
			$("#inputForm input[name='check_action']").each(function(i){
				$(this).bind("click",function(){
					if(i==0){
						//审核通过
						$("tr[name^='check_action_tr']").hide();
						$("#inputForm input[name='action']").val("check_action_1");
						loading('正在提交，请稍等...');
						$("#inputForm").submit();
					}else if(i==1){
						//审核不通过
						$("tr[name^='check_action_tr']").add("#check_action").show();
						$("#check_action").bind("click",function(){
							$("#inputForm input[name='action']").val("check_action_0");
							if (CKEDITOR.instances.uncheckedReason.getData()=="") {
								top.$.jBox.tip('请填写退回理由', 'warning');
								return;
							}
							loading('正在提交，请稍等...');
							$("#inputForm").submit();
						})
					}
				});
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<%--<li><a href="${ctx}/publicoption/publicOpinion/">舆情监控列表</a></li>--%>
		<li class="active">
			<%--<a href="${ctx}/publicoption/publicOpinion/form?id=${publicOpinion.id}">舆情监控<shiro:hasPermission name="publicoption:publicOpinion:edit">${not empty publicOpinion.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="publicoption:publicOpinion:edit">查看</shiro:lacksPermission></a>--%>
			<a href="${ctx}/publicoption/publicOpinion/form?id=${publicOpinion.id}">舆情监控详情</a>
		</li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="publicOpinion" action="${ctx}/publicoption/publicOpinion/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="action"/>
		<form:hidden path="act.taskId"/>
		<sys:message content="${message}"/>
	    <fieldset>
		<legend>舆情信息</legend>
		<table class="table-form">
			<tr>
				<td class="tit">标题</td>
				<td colspan="3">${publicOpinion.title}</td>
				<td class="tit">采集源</td>
				<td >${publicOpinion.collectOrigin}</td>
			</tr>
			<tr>
				<td class="tit">发布时间</td>
				<td>${publicOpinion.releaseTime}</td>
				<td class="tit">源地址</td>
				<td colspan="3"><a href="${publicOpinion.originUrl}" target="_blank">${publicOpinion.originUrl}</a></td>
			</tr>
			<tr>
				<td class="tit">内容</td>
				<td colspan="5">
					<div style="margin: 10px;"> ${publicOpinion.content}</div>
				</td>
			</tr>
			<tr>
				<td class="tit">浏览量</td>
				<td>${publicOpinion.viewCount}</td>
				<td class="tit">转发量</td>
				<td>${publicOpinion.transCount}</td>
				<td class="tit">回复量</td>
				<td>${publicOpinion.replyCount}</td>
			</tr>
			<tr>
				<td class="tit">涉及领域</td>
				<td>${fns:getDictLabel(publicOpinion.involveField, 'public_opinion_field', '')}</td>
				<td class="tit">涉及街镇</td>
				<td>${publicOpinion.involveTownName}</td>
				<td class="tit">状态</td>
				<td>${fns:getDictLabel(publicOpinion.status, 'public_opinion_sataus', '')}</td>
			</tr>
			<c:if test="${not empty publicOpinion.synergylist}">
			<tr>
				<td class="tit">协办信息</td>
				<td colspan="5">
					<c:forEach items="${publicOpinion.synergylist}" var="synergy">
						<table class="table-form">
							<tr>
								<td class="tit">协办内容</td>
								<td colspan="5">
									<div style="margin: 10px;">${synergy.synergizeContent}</div>
								</td>
							</tr>
							<tr>
								<td class="tit">协办时间</td>
								<td><fmt:formatDate value="${synergy.synergizeDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td class="tit">协办单位</td>
								<td>${synergy.synergizeDeptCode}</td>
								<td class="tit">附件</td>
								<td>${synergy.synergizeAtta}</td>
							</tr>
						</table>
					</c:forEach>
				</td>
			</tr>
			</c:if>
			<c:if test="${not empty publicOpinion.checklist}">
				<tr>
					<td class="tit">审核信息</td>
					<td colspan="5">
						<c:forEach items="${publicOpinion.checklist}" var="check">
							<table class="table-form">
								<tr>
									<td class="tit">退回说明</td>
									<td colspan="5">
										<div style="margin: 10px;">${check.reason}</div>
									</td>
								</tr>
							</table>
						</c:forEach>
					</td>
				</tr>
			</c:if>

			<!-- 分发 start -->
			<c:if test="${publicOpinion.action eq 'hand_out'}">
			<tr>
				<td class="tit">分发单位</td>
				<td colspan="5">
					<sys:treeselect id="hdept" name="office.id" value="" labelName="office.name" labelValue=""
									title="部门" url="/sys/office/treeData?type=2&isAll=true" cssClass="input-xlarge" allowClear="true" notAllowSelectParent="true"/>
				</td>
			</tr>
			</c:if>
			<!-- 分发 end -->

			<!-- 递交审核 要求协办 退回  start-->
			<c:if test="${publicOpinion.action eq 'deal_with'}">
			<tr>
				<td class="tit">办理方式</td>
				<td colspan="5">
					<input name ="next_action" type="radio">回复
					<input name ="next_action" type="radio">协办
					<input name ="next_action" type="radio">退回
				</td>
			</tr>
			</c:if>
			<!-- 递交审核 要求协办 退回  end-->

			<!-- 递交审核 start -->
			<tr name="deal_with_1_tr_1" style="display: none;">
				<td class="tit">回复内容</td>
				<td colspan="5">
					<form:textarea id="replyContent" htmlEscape="true" path="replyContent" rows="4" maxlength="200" class="input-xxlarge"/>
					<sys:ckeditor replace="replyContent" uploadPath="/cms/publicOpinion" />
				</td>
			</tr>
			<tr name="deal_with_1_tr_2" style="display: none;">
				<td class="tit">回复证明</td>
				<td colspan="5">
					<input type="hidden" id="replyProve" name="replyProve" value="${publicOpinion.replyProve}" />
					<sys:ckfinder input="replyProve" type="thumb" uploadPath="/cms/publicOpinion" selectMultiple="false"/>
				</td>
			</tr>
			<!-- 递交审核 end -->
 c
			<!-- 要求协办start -->
			<tr name="deal_with_2_tr_1" style="display: none;">
				<td class="tit">协办单位</td>
				<td colspan="5">
					<sys:treeselect id="hdept" name="office.id" value="" labelName="office.name" labelValue=""
									title="部门" url="/sys/office/treeData?type=2&isAll=true" cssClass="input-xlarge" allowClear="true" notAllowSelectParent="true"/>
				</td>
			</tr>
			<!-- 要求协办end -->

			<!-- 协办start -->
			<c:if test="${publicOpinion.action eq 'join_hands'}">
			<tr>
				<td class="tit">协办内容</td>
				<td colspan="5">
					<form:textarea id="synergizeContent" htmlEscape="true" path="synergizeContent" rows="4" maxlength="200" class="input-xxlarge"/>
					<sys:ckeditor replace="synergizeContent" uploadPath="/cms/publicOpinion" />
				</td>
			</tr>
			<tr>
				<td class="tit">上传附件</td>
				<td colspan="5">
					<input type="hidden" id="synergizeAtta" name="synergizeAtta" value="${synergizeAtta}" />
					<sys:ckfinder input="synergizeAtta" type="thumb" uploadPath="/cms/publicOpinion" selectMultiple="false"/>
				</td>
			</tr>
			</c:if>
			<!-- 协办end -->


			<!-- 审核 start -->
			<c:if test="${publicOpinion.action eq 'to_check'}">
				<tr>
					<td class="tit">回复内容</td>
					<td colspan="5">${publicOpinion.replyContent}</td>
				</tr>
				<tr>
					<td class="tit">回复证明</td>
					<td colspan="5">
							<a href="${publicOpinion.replyProve}" target="_blank">查看</a>
					</td>
				</tr>
				<tr>
					<td class="tit">审核结果</td>
					<td colspan="5">
						<input name ="check_action" type="radio">通过
						<input name ="check_action" type="radio">不通过
					</td>
				</tr>
				<tr name="check_action_tr_1" style="display: none;">
					<td class="tit">退回原因</td>
					<td colspan="5">
						<form:textarea id="uncheckedReason" htmlEscape="true" path="uncheckedReason" rows="4" maxlength="200" class="input-xxlarge"/>
						<sys:ckeditor replace="uncheckedReason" uploadPath="/cms/publicOpinion" />
					</td>
				</tr>
			</c:if>
			<!-- 审核 end -->

		</table>
		</fieldset>
		<act:histoicFlow procInsId="${publicOpinion.processInstanceId}" />
		<div class="form-actions">
			<shiro:hasPermission name="publicoption:publicOpinion:edit">
				<c:if test="${publicOpinion.action eq 'hand_out'}">
					<input id="hand_out"  name="btnSubmit" class="btn btn-primary" type="button" value="分发"/>&nbsp;
                </c:if>
				<input id="deal_with_1" name="btnSubmit" class="btn btn-primary" type="button" value="提交审核" style="display: none;"/>&nbsp;
				<input id="deal_with_2" name="btnSubmit" class="btn btn-primary" type="button" value="申请协办" style="display: none;"/>&nbsp;
				<c:if test="${publicOpinion.action eq 'join_hands'}">
					<input id="join_hands"  name="btnSubmit" class="btn btn-primary" type="button" value="完成协办"/>&nbsp;
				</c:if>
				<c:if test="${publicOpinion.action eq 'to_check'}">
				<input id="check_action" name="btnSubmit" class="btn btn-primary" type="button" value="退回" style="display: none;"/>&nbsp;
				</c:if>
			</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>