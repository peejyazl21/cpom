<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>舆情监控管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#btnExportExcel").bind("click",function(){
				var html = "<br/><label>&nbsp;&nbsp;开始时间：</label>";
				html +=	'<input id="beginDate" name="beginDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate" style="width:163px;" value="" onclick="WdatePicker({dateFmt:\'yyyy-MM-dd\'});">';
				html += "<br/><label>&nbsp;&nbsp;结束时间：</label>";
				html += '<input id="endDate" name="endDate" type="text" readonly="readonly" maxlength="20" class="input-medium Wdate" style="width:163px;" value="" onclick="WdatePicker({dateFmt:\'yyyy-MM-dd\'});">';
				html += "<br/><label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;已回复：</label>";
				html += "是<input type='radio' name='status' checked/>&nbsp;&nbsp;&nbsp;&nbsp;否<input type='radio' name='status'/>";
				var submit = function (v, h, f) {

					debugger;
					if (f.beginDate == '') {
						$.jBox.tip("请输入开始时间", 'error', { focusId: "beginDate" });
						return false;
					}
					if (f.endDate == '') {
						$.jBox.tip("请输入结束时间", 'error', { focusId: "endDate" });
						return false;
					}
					//提交办理请求
					var status = $("input[name='status']:checked").val();
					$.ajax({
						type:"post",
						url : "${ctx}/publicoption/publicOpinion/exportExcel",
						dataType:"json",
						data:{beginDate:f.endDate,endDate:f.endDate,status:status},
						success:function(result){
							$.jBox.tip('已经开始导出数据！');
							location.href=result;
						}
					});
				};
				$.jBox(html, { title: "请选时间范围", submit: submit });
			});
		});




		function page(n,s){
			$("#pageNo").val(n);
			$("#pageSize").val(s);
			$("#searchForm").submit();
        	return false;
        }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/publicoption/publicOpinion/list">舆情监控列表</a></li>
		<li><a href="${ctx}/publicoption/publicOpinion/addPublicOpinion">添加舆情监控</a></li>
	</ul>
	<form:form id="searchForm" modelAttribute="publicOpinion" action="${ctx}/publicoption/publicOpinion/list" method="post" class="breadcrumb form-search">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<ul class="ul-form">
			<li><label>标题：</label>
				<form:input path="title" htmlEscape="false" maxlength="200" class="input-medium"/>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询"/></li>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<li class="btns"><input id="btnExportExcel" class="btn btn-primary" type="button" value="导出数据"/></li>
			<li class="clearfix"></li>
		</ul>
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>标题</th>
				<th>来源</th>
				<th>发布时间</th>
				<th>涉及领域</th>
				<th>涉及街镇</th>
				<th>状态</th>
				<th>浏览量</th>
				<th>转发量</th>
				<th>回复量</th>
				<shiro:hasPermission name="publicoption:publicOpinion:edit"><th>操作</th></shiro:hasPermission>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="publicOpinion">
			<tr>
				<td><a href="${ctx}/publicoption/publicOpinion/form?id=${publicOpinion.id}">
					${publicOpinion.title}
				</a></td>
				<td>${publicOpinion.collectOrigin}</td>
				<td>${publicOpinion.releaseTime}</td>
				<td>
					${fns:getDictLabel(publicOpinion.involveField, 'public_opinion_field', '')}
				</td>
				<td>
					${publicOpinion.involveTownName}
				</td>
				<td>
					${fns:getDictLabel(publicOpinion.status, 'public_opinion_sataus', '')}
				</td>
				<td>${publicOpinion.viewCount}</td>
				<td>${publicOpinion.transCount}</td>
				<td>${publicOpinion.replyCount}</td>
				<td>
					<shiro:hasPermission name="publicoption:publicOpinion:edit">
						<a href="${ctx}/publicoption/publicOpinion/delete?id=${publicOpinion.id}" onclick="return confirmx('确认要删除该舆情监控吗？', this.href)">删除</a>
					</shiro:hasPermission>
					<a href="${publicOpinion.originUrl}" target="_blank">回溯</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>