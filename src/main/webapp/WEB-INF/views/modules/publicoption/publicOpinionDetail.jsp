<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>舆情监控详情</title>
	<meta name="decorator" content="default"/>
</head>
<body>
	<ul class="nav nav-tabs">
		<%--<li><a href="${ctx}/publicoption/publicOpinion/">舆情监控列表</a></li>--%>
		<li class="active">
			<%--<a href="${ctx}/publicoption/publicOpinion/form?id=${publicOpinion.id}">舆情监控<shiro:hasPermission name="publicoption:publicOpinion:edit">${not empty publicOpinion.id?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="publicoption:publicOpinion:edit">查看</shiro:lacksPermission></a>--%>
			<a href="${ctx}/publicoption/publicOpinion/form?id=${publicOpinion.id}">舆情监控详情</a>
			<li><a href="${ctx}/publicoption/publicOpinion/addPublicOpinion">添加舆情监控</a></li>
			</li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="publicOpinion" action="${ctx}/publicoption/publicOpinion/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="action"/>
		<form:hidden path="act.taskId"/>
		<sys:message content="${message}"/>
	    <fieldset>
		<legend>舆情信息</legend>
		<table class="table-form">
			<tr>
				<td class="tit">标题</td>
				<td colspan="3">${publicOpinion.title}</td>
				<td class="tit">采集源</td>
				<td >${publicOpinion.collectOrigin}</td>
			</tr>
			<tr>
				<td class="tit">发布时间</td>
				<td>${publicOpinion.releaseTime}</td>
				<td class="tit">源地址</td>
				<td colspan="3"><a href="${publicOpinion.originUrl}" target="_blank">${publicOpinion.originUrl}</a></td>
			</tr>
			<tr>
				<td class="tit">内容</td>
				<td colspan="5">
					<div style="margin: 10px;"> ${publicOpinion.content}</div>
				</td>
			</tr>
			<tr>
				<td class="tit">浏览量</td>
				<td>${publicOpinion.viewCount}</td>
				<td class="tit">转发量</td>
				<td>${publicOpinion.transCount}</td>
				<td class="tit">回复量</td>
				<td>${publicOpinion.replyCount}</td>
			</tr>
			<tr>
				<td class="tit">涉及领域</td>
				<td>${fns:getDictLabel(publicOpinion.involveField, 'public_opinion_field', '')}</td>
				<td class="tit">涉及街镇</td>
				<td>${publicOpinion.involveTownName}</td>
				<td class="tit">状态</td>
				<td>${fns:getDictLabel(publicOpinion.status, 'public_opinion_sataus', '')}</td>
			</tr>
			<c:if test="${not empty publicOpinion.synergylist}">
			<tr>
				<td class="tit">协办信息</td>
				<td colspan="5">
					<c:forEach items="${publicOpinion.synergylist}" var="synergy">
						<table class="table-form">
							<tr>
								<td class="tit">协办内容</td>
								<td colspan="5">
									<div style="margin: 10px;">${synergy.synergizeContent}</div>
								</td>
							</tr>
							<tr>
								<td class="tit">协办时间</td>
								<td><fmt:formatDate value="${synergy.synergizeDate}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
								<td class="tit">协办单位</td>
								<td>${synergy.synergizeDeptCode}</td>
								<td class="tit">附件</td>
								<td>${synergy.synergizeAtta}</td>
							</tr>
						</table>
					</c:forEach>
				</td>
			</tr>
			</c:if>
			<c:if test="${not empty publicOpinion.checklist}">
				<tr>
					<td class="tit">审核信息</td>
					<td colspan="5">
						<c:forEach items="${publicOpinion.checklist}" var="check">
							<table class="table-form">
								<tr>
									<td class="tit">退回说明</td>
									<td colspan="5">
										<div style="margin: 10px;">${check.reason}</div>
									</td>
								</tr>
							</table>
						</c:forEach>
					</td>
				</tr>
			</c:if>
		</table>
		</fieldset>
		<act:histoicFlow procInsId="${publicOpinion.processInstanceId}" />
		<div class="form-actions">
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>