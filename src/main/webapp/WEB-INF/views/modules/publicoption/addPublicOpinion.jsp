<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
    <title>舆情监控添加</title>
    <meta name="decorator" content="default"/>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#publicOpinionForm").validate({
                submitHandler: function(form){
                    loading('正在提交，请稍等...');
                    form.submit();
                },
                errorContainer: "#messageBox",
                errorPlacement: function(error, element) {
                    $("#messageBox").text("输入有误，请先更正。");
                    if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
                        error.appendTo(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        })
    </script>
</head>
<body>
    <ul class="nav nav-tabs">
        <li><a href="${ctx}/publicoption/publicOpinion/list">舆情监控列表</a></li>
        <li class="active"><a href="${ctx}/publicoption/publicOpinion/addPublicOpinion">添加舆情监控</a></li>
    </ul>

    <form:form modelAttribute="publicOpinion" class="form-horizontal"
               id="publicOpinionForm" method="post"
               action="${ctx}/publicoption/publicOpinion/addPublicOpinionForm">
        <form:hidden path="id"/>

        <span >标&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;题&nbsp;:</span>
        <form:input path="title" cssStyle="width: 250px;" htmlEscape="false" maxlength="2000" class="input-xxlarge required"/>
        &nbsp;&nbsp;
        <span >采&nbsp;&nbsp;&nbsp;集&nbsp;&nbsp;&nbsp;源&nbsp;:</span>
        <form:input path="collectOrigin" cssStyle="width: 250px;" htmlEscape="false" maxlength="1000" class="input-xxlarge"/>
        <%--<div class="control-group">
            <label class="control-label">标题:</label>
            <div class="controls">
                <form:input path="title" cssStyle="width: 250px;" htmlEscape="false" maxlength="1000" class="input-xxlarge required"/>
                <span class="help-inline"><font color="red">*</font> </span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">采集源:</label>
            <div class="control-group">
            <div class="controls">
                <form:input path="collectOrigin" cssStyle="width: 250px;" htmlEscape="false" maxlength="1000" class="input-xxlarge"/>
            </div>
            </div>
        </div>--%>

        <hr>
        <span>发&nbsp;布&nbsp;时&nbsp;间&nbsp;:</span>
        <input id="releaseTime"  name="releaseTime"  type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" style="width:236px;"
               value="<fmt:formatDate value="${publicOpinion.releaseTime}" pattern="yyyy-MM-dd"/>"
               onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
        <span class="help-inline"><font color="red">*</font> </span>&nbsp;&nbsp;
        <span>源&nbsp;&nbsp;地&nbsp;&nbsp;址&nbsp;:</span>
        <form:input path="originUrl" htmlEscape="false" cssStyle="width: 250px;" maxlength="2000" class="input-xxlarge required"/>

        <hr>
        <span>内&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;容&nbsp;:</span>
        <form:textarea path="content" htmlEscape="false" rows="3" maxlength="200" class="input-xxlarge required"/>
        <span class="help-inline"><font color="red">*</font> </span>

        <hr>
        <span>涉&nbsp;及&nbsp;领&nbsp;域&nbsp;:</span>
        <form:select path="involveField" class="input-medium">
            <form:option value="" label="涉及领域"/>
            <form:options items="${fns:getDictList('public_opinion_field')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
        </form:select>
        &nbsp;&nbsp;
        <span>涉&nbsp;及&nbsp;街&nbsp;区&nbsp;:</span>
        <form:input path="involveTownName" cssStyle="width: 150px;"  htmlEscape="false" maxlength="2000" class="input-xlarge required"/>
        &nbsp;&nbsp;
        <span>状&nbsp;&nbsp;&nbsp;&nbsp;态&nbsp;:</span>
        <form:select path="status" class="input-medium">
            <form:option value="" label="状态"/>
            <form:options items="${fns:getDictList('public_opinion_sataus')}" itemLabel="label" itemValue="value"  htmlEscape="false"/>
        </form:select>
        <%--
          <div  class="control-group">
            <label class="control-label">涉及街区：&nbsp;</label>
            <div class="controls">
                <form:input path="involveTownName"  htmlEscape="false" maxlength="2000" class="input-xlarge required"/>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">发布时间:</label>
            <div class="controls">
                <input id="releaseTime"  name="releaseTime"  type="text" readonly="readonly" maxlength="20" class="input-medium Wdate required" style="width:149px;"
                       value="<fmt:formatDate value="${publicOpinion.releaseTime}" pattern="yyyy-MM-dd"/>"
                       onclick="WdatePicker({dateFmt:'yyyy-MM-dd'});"/>
                <span class="help-inline"><font color="red">*</font> </span>
            </div>
        </div>--%>

        <%--<div  class="control-group">
            <label class="control-label">涉及领域：&nbsp;</label>
            <div class="controls">
                <form:select path="involveField" class="input-medium">
                    <form:option value="" label="涉及领域"/>
                    <form:options items="${fns:getDictList('public_opinion_field')}" itemLabel="label" itemValue="value" htmlEscape="false"/>
                </form:select>
            </div>
        </div>--%>

        <%--<div  class="control-group">
            <label class="control-label">状态：&nbsp;</label>
            <div class="controls">
                <form:select path="status" class="input-medium">
                    <form:option value="" label="状态"/>
                    <form:options items="${fns:getDictList('public_opinion_sataus')}" itemLabel="label" itemValue="value"  htmlEscape="false"/>
                </form:select>
            </div>
        </div>--%>

       <%-- <div class="control-group">
            <label class="control-label">源地址:</label>
            <div class="controls">
                <form:input path="originUrl" htmlEscape="false" maxlength="2000" class="input-xxlarge required"/>
                <span class="help-inline"><font color="red">*</font> </span>
            </div>
        </div>--%>

        <%--<div class="control-group">
            <label class="control-label">内容:</label>
            <div class="controls">
                <form:textarea path="content" htmlEscape="false" rows="3" maxlength="200" class="input-xxlarge required"/>
                <span class="help-inline"><font color="red">*</font> </span>
            </div>
        </div>--%>

        <div class="form-actions">
            <input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>
            &nbsp;<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
        </div>

    </form:form>


</body>
</html>
